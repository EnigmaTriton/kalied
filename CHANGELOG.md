# Changelog

## [master] - 2024-02-19

Initial public push with full implementation for code generation compatible with Wireshark until version 4.4.
