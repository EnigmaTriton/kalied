# Contributing to Kalied

Thank you for your interest in contributing to Kalied!

Since the project is revolving around Wireshark, the recommendations for contributions are essentially the same.

Bug reports, feature requests, pull requests are obviously welcome.

Documentation is lacking, as well as CI/CD setup, if anyone is interested in improving on these fronts, I would gladly help them understand the project. 🙏
