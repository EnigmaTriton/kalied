# Kalied

Kalied is the name for the Sea Thrift (Armeria Maritima) in Breton language.

It is also the name of this code generator of Wireshark dissectors for Apache Thrift-based protocols.

## Build Kalied

```sh
cmake -S kalied -B build-kalied && \
    cd build-kalied && \
    make
```

## Usage

```sh
./kalied path/to/awesomeproto.thrift
```

It will generate `packet-awesomeproto.c` which is the source of the dissector itself.

Complete documentation is available with `./kalied --help`

## Dissector compilation and Wireshark integration

### As a plugin

1. Make sure `packet-awesomeproto.c` is generated with the `--plugin` flag (or without the `--builtin` flag as `--plugin` is the default).
2. Create a directory named `plugins/epan/awesomeproto` (adapt to the name of your protocol, of course) and put the generated file in it.
3. Add `AUTHORS.md`, `CHANGELOG.md` and `README.md` as you see fit.
4. Create `CMakeLists.txt` and `plugin.rc.in` tailored for your protocol (get inspiration from other plugins, the files are quite small).
5. Create a simple `CMakeListsCustom.txt` in the root directory of Wireshark sources (see below).
6. Build Wireshark.

```CMake
set(CUSTOM_PLUGIN_SRC_DIR
        plugins/epan/awesomeproto
)
```

### As an integrated dissector

1. Generate the source code with the `--builtin` flag.
2. Put the generated file in `epan/dissectors` directory.
3. Add the file in `epan/dissectors/CMakeLists.txt` under the form `${CMAKE_CURRENT_SOURCE_DIR}/packet-awesomeproto.c`
4. Build Wireshark.
