#include <assert.h>
#include <glib.h>
#include <stdio.h>
#include "entry_points.h"

static const uint32_t basic_depth = 1;

typedef enum _parent_t {
    RESULT,
    PARAMETER,
    EXCEPTION,
    STRUCT,
    CONTAINER,
    TYPEDEF,
} parent_t;

typedef struct _context_t {
    tip_types_t* types;  // Types for the protocol, same throughout and updated along.
    proto_t*     proto;  // Protocol parameters, also same (but updated).
    hf_data_t*   parent; // hf_data_t header of the parent element
                         // for a field, it’s the struct, the union or the exception
                         // for a result, a parameter or a throw, it’s the function
                         // for a key or a value, it’s the map
                         // for a named type, it’s the user that only know its identifier (a field, a parameter, a key, …)
    parent_t     ptype;  // What’s the type of the parent to know how to handle the current object.
    uint32_t     depth;  // Used by the child to pass it’s own depth back to its parent on GSList traversal.
    char*        kve_id; // When the parent is a CONTAINER, name part to use for the code generation ("result_element" if the return type is a list)
    char*        kve_hf; // When the parent is a CONTAINER, filter part to use ("result.element" in the same use case).
} context_t;

static uint32_t
find_and_annotate(tip_field_t* field, context_t* context);

static uint32_t
max_depth(uint32_t current, uint32_t next)
{   // In this case, 0 means infinity as a basic type depth is 1 (even for void).
    if (current == 0 || next == 0) {
        return 0;
    }
    return current > next ? current : next;
}

static uint32_t
increment_depth(uint32_t depth) {
    if (depth == 0) {
        return 0;
    }
    return depth + 1;
}

char*
element_name(const char* parent, const char* child, bool lowercase, char separator)
{
    char* name = g_strdup_printf("%s%c%s", parent, separator, child);
    if (lowercase) {
        g_ascii_strdown(name, -1);
    }
    return name;
}

static uint32_t
annotate_field(tip_field_t* field, context_t* context)
{
    assert(field != NULL);
    assert(context != NULL);
    uint32_t depth = basic_depth;
    hf_data_t* parent = context->parent;
    context_t child_ctx = {
        .types = context->types,
        .proto = context->proto,
        .parent = &(field->hf_data),
    };
    bool lowercase = context->proto->lowercase;
    if (context->ptype == CONTAINER) {
        field->hf_data.is_contained = true;
    }
    if (field->hf_data.is_used) {
        // Already checked, used the remembered value.
        return field->max_depth;
    }
    field->hf_data.is_used = true;
    if (field->_is_set.type) {
        switch (context->ptype) {
        case CONTAINER:
            // If type is already set, it means that it’s a basic type or another container.
            // Then, we copy the identifier and filter handed over by the parent as there is no other name available.
            field->hf_data.identifier = g_strdup(context->kve_id);
            field->type_name = g_strdup(context->kve_hf);
            break;
        case RESULT:
            // A return type does not have a name, get the generic one from the configuration.
            field->hf_data.identifier = context->proto->return_filter;
            field->type_name = context->proto->return_filter;
            break;
        default:
            // Proceed normally for everything else.
            break;
        }
        switch (field->type) {
        case DE_THRIFT_T_VOID:
        case DE_THRIFT_T_BOOL:
        case DE_THRIFT_T_I8:
        case DE_THRIFT_T_DOUBLE:
        case DE_THRIFT_T_I16:
        case DE_THRIFT_T_I32:
        case DE_THRIFT_T_I64:
        case DE_THRIFT_T_UUID:
            break;
        case DE_THRIFT_T_BINARY:
            if (field->_is_set.alt_type) {
                context->types->has_string = true;
            } else {
                context->types->has_binary = true;
            }
            break;
        case DE_THRIFT_T_MAP:
            child_ctx.ptype = CONTAINER;
            child_ctx.kve_id = element_name(field->hf_data.identifier, map_key, lowercase, UNDERSCORE);
            child_ctx.kve_hf = element_name(field->hf_data.identifier, map_key, lowercase, DOT);
            depth = increment_depth(annotate_field(field->key, &child_ctx));
            g_free(context->kve_id);
            g_free(context->kve_hf);
            child_ctx.kve_id = element_name(field->hf_data.identifier, map_value, lowercase, UNDERSCORE);
            child_ctx.kve_hf = element_name(field->hf_data.identifier, map_value, lowercase, DOT);
            depth = max_depth(depth, increment_depth(annotate_field(field->value, &child_ctx)));
            g_free(context->kve_id);
            g_free(context->kve_hf);
            child_ctx.kve_id = NULL;
            child_ctx.kve_hf = NULL;
            break;
        case DE_THRIFT_T_SET:
        case DE_THRIFT_T_LIST:
            child_ctx.ptype = CONTAINER;
            child_ctx.kve_id = element_name(field->hf_data.identifier, ls_elt, lowercase, UNDERSCORE);
            child_ctx.kve_hf = element_name(field->hf_data.identifier, ls_elt, lowercase, DOT);
            depth = max_depth(depth, increment_depth(annotate_field(field->value, &child_ctx)));
            g_free(context->kve_id);
            g_free(context->kve_hf);
            child_ctx.kve_id = NULL;
            child_ctx.kve_hf = NULL;
            break;
        default:
            fprintf(stderr, "Unsupported field type: %d.\n", field->type);
            depth = 0;
        }
    } else {
        // We want to find the real type for which we just know the identifier.
        // The context of the found type is the current context.
        // In particular the parent of the found type will be the parent of the current object.
        depth = find_and_annotate(field, context);
    }
    if (context->ptype == RESULT && field->hf_data.identifier == NULL) {
        field->hf_data.identifier = context->proto->return_filter;
        field->type_name = context->proto->return_filter;
    }
    // Remember the value to avoid repetitive traversals.
    field->max_depth = depth;
    return depth;
}

void
annotate_inner_field(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    tip_field_t* field = value;
    context_t* context = data;
    context->depth = max_depth(context->depth, annotate_field(field, context));
}

static void
annotate_enum_values(void* value, void* data)
{
    tip_const_value_t* enum_val = value;
    int32_t* current = data;
    if (enum_val->implicit) {
        enum_val->int_value = *current;
    }
    *current = enum_val->int_value + 1;
}


static uint32_t
annotate_enumeration(tip_enum_t* value, context_t* context)
{
    assert(value != NULL);
    assert(context != NULL);
    int32_t current = 0;
    if (context->ptype == CONTAINER) {
        value->hf_data.is_contained = true;
    }
    if (value->hf_data.is_used) {
        return basic_depth;
    }
    value->hf_data.is_used = true;
    g_slist_foreach(value->values, annotate_enum_values, &current);
    return basic_depth;
}

static uint32_t
annotate_structure(tip_struct_t* value, context_t* context)
{
    assert(value != NULL);
    assert(context != NULL);
    assert(context->types != NULL);
    context_t field_context = {
        .types = context->types,
        .proto = context->proto,
        .parent = &(value->hf_data),
        .ptype = STRUCT,
        .depth = basic_depth,
    };
    if (context->ptype == CONTAINER) {
        value->hf_data.is_contained = true;
    }
    if (value->hf_data.is_used) {
        return value->max_depth;
    }
    value->hf_data.is_used = true;
    g_slist_foreach(value->fields, annotate_inner_field, &field_context);
    if (value->is_union) {
        context->types->has_union = true;
    }
    // We still increment for unions because we are calculating the depth
    // for the generic dissector in case of issue, which means that there
    // will always be a sub-tree.
    value->max_depth = increment_depth(field_context.depth);
    return value->max_depth;
}

static uint32_t
find_and_annotate(tip_field_t* field, context_t* context)
{
    char* key = field->type_name;
    void* value;
    uint32_t depth = 0;
    assert(context != NULL);
    assert(context->types != NULL);
    assert(field != NULL);
    // Determine if the type is already in a namespace.
    if (strchr(key, '.') == NULL) {
        key = full_name(field->hf_data.file_name, field->type_name);
    }
    if (context->ptype == EXCEPTION) {
        value = g_hash_table_lookup(context->types->l.exceptions, key);
        if (value) {
            depth = annotate_structure(value, context);
            field->type = DE_THRIFT_T_STRUCT;
            field->_is_set.type = true;
            field->hf_data.target_type = value;
        } else {
            // TODO: Handle missing exception -> can be done in structure generation.
            fprintf(stderr, "Exception %s not found.\n", key);
        }
        goto faa_return;
    }
    value = g_hash_table_lookup(context->types->l.enumerations, key);
    if (value) {
        depth = annotate_enumeration(value, context);
        field->type = DE_THRIFT_T_I32;
        field->_is_set.type = true;
        field->_is_set.alt_type = true;
        field->hf_data.target_type = value;
        goto faa_return;
    }
    value = g_hash_table_lookup(context->types->l.structures, key);
    if (value) {
        tip_struct_t* structure = value;
        depth = annotate_structure(structure, context);
        field->type = DE_THRIFT_T_STRUCT;
        field->_is_set.type = true;
        field->_is_set.alt_type = structure->is_union;
        field->hf_data.target_type = value;
        goto faa_return;
    }
    value = g_hash_table_lookup(context->types->l.typedefs, key);
    if (value) {
        tip_field_t* target = value;
        depth = annotate_field(target, context);
        assert(target->_is_set.type);
        field->type = target->type;
        field->_is_set.type = true;
        field->_is_set.alt_type = target->_is_set.alt_type;
        field->hf_data.target_type = value;
        goto faa_return;
    }
    field->type = DE_THRIFT_T_GENERIC;
    field->_is_set.type = true;
    fprintf(stderr, "Type %s not found.\n", key);
faa_return:
    if (key != field->type_name) {
        g_free(key);
    }
    //assert(field->_is_set.type);
    return depth;
}

void
annotate_used_depth(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    tip_function_t* func = value;
    annotation_data_t* params = data;
    func->hf_data.is_used = true;
    context_t context = {
        .types = params->types,
        .proto = params->proto,
        .ptype = RESULT,
        .parent = value,
    };
    context.depth = annotate_field(func->result, &context);
    context.ptype = EXCEPTION;
    g_slist_foreach(func->throws, annotate_inner_field, &context);
    // For the generated union containing the result or exception.
    context.depth += 1u; // TODO: Disable sub-tree generation in generic dissector.
    context.ptype = PARAMETER;
    g_slist_foreach(func->params, annotate_inner_field, &context);
    func->max_depth = context.depth;
}

