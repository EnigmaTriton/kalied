#include <glib.h>
#include <assert.h>

#include "def_types.h"
#include "entry_points.h"

void
free_token(thrift_idl_token_t* token)
{
    g_free(token->v);
    g_free(token);
}

static void
free_string(void* value, void* data)
{
    g_free(value);
}

static void
free_hf_data(void* value)
{
    hf_data_t* hf_data = value;
    if (hf_data->identifier != NULL) {
        g_free(hf_data->identifier);
    }
}

static void
free_enum_value(void* value, void* data)
{
    free_hf_data(value);
    g_free(value);
}

static void
free_enumeration(void* value, void* data)
{
    tip_enum_t* enumeration = value;
    free_hf_data(value);
    g_slist_foreach(enumeration->values, free_enum_value, NULL);
    g_slist_free(enumeration->values);
    g_free(value);
}

static void
free_field(void* value, void* data)
{
    tip_field_t* field = value;
    if (field->type_name == NULL) {
        // If type_name is set, then the identifier is only a reference.
        free_hf_data(value);
    }
    if (!field->_is_set.type) {
        assert(field->type == DE_THRIFT_T_STOP);
    }
    switch (field->type) {
    case DE_THRIFT_T_MAP:
        free_field(field->key, NULL);
        free_field(field->value, NULL);
        break;
    case DE_THRIFT_T_SET:
    case DE_THRIFT_T_LIST:
        free_field(field->value, NULL);
        break;
    default:
        // nothing to do in for other types.
        break;
    }
    g_free(value);
}

static void
free_structure(void* value, void* data)
{
    tip_struct_t* structure = value;
    free_hf_data(value);
    g_slist_foreach(structure->fields, free_field, NULL);
    g_slist_free(structure->fields);
    g_free(value);
}

static void
free_function(void* value, void* data)
{
    tip_function_t* func = value;
    free_hf_data(value);
    g_slist_foreach(func->params, free_field, NULL);
    g_slist_free(func->params);
    free_field(func->result, NULL);
    g_slist_foreach(func->throws, free_field, NULL);
    g_slist_free(func->throws);
    g_free(value);
}

void
final_cleanup(GSList* file_list, proto_t* proto, tip_types_t* types)
{
    g_slist_foreach(file_list, free_string, NULL);
    g_slist_free(file_list);
    g_free(proto->author);
    g_free(proto->email);
    g_free(proto->name);
    g_free(proto->short_name);
    g_free(proto->abbrev);
    g_free(proto->year);
    g_free(proto->spdx);
    g_free(proto->return_filter);
    g_slist_foreach(types->l.common_strings, free_string, NULL);
    g_slist_free(types->l.common_strings);
    g_slist_foreach(types->typedefs, free_field, NULL);
    g_slist_free(types->typedefs);
    g_hash_table_destroy(types->l.typedefs);
    g_slist_foreach(types->enumerations, free_enumeration, NULL);
    g_slist_free(types->enumerations);
    g_hash_table_destroy(types->l.enumerations);
    g_slist_foreach(types->structures, free_structure, NULL);
    g_slist_free(types->structures);
    g_hash_table_destroy(types->l.structures);
    g_slist_foreach(types->exceptions, free_structure, NULL);
    g_slist_free(types->exceptions);
    g_hash_table_destroy(types->l.exceptions);
    g_slist_foreach(types->functions, free_function, NULL);
    g_slist_free(types->functions);
}
