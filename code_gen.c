#include <assert.h>
#include <glib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>

#include "entry_points.h"
/* IDL Brouillon

    struct configuration {}
    enum cfg_id {}
    union variant {}
    struct settings {
        map<cfg_id, configuration> config;
        map<string, list<variant> capabilities;
    }

    set_all(map<cfg_id, configuration> config, map<string, list<variant> capabilities);
    map<cfg_id, configuration> get_config();
    map<string, list<variant> get_capabilities();
*/

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                 Enumerations and structures                               //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
static char* lowercase[] = {
    "/*generic*/",
    "stop",
    "void",
    "bool",
    "i8",
    "double",
    "unused_5",
    "i16",
    "unused_7",
    "i32",
    "unused_9",
    "i64",
    "binary",
    "struct",
    "map",
    "set",
    "list",
    "uuid",
};

static char* uppercase[] = {
    "GENERIC",
    "STOP",
    "VOID",
    "BOOL",
    "I8",
    "DOUBLE",
    "UNUSED_5",
    "I16",
    "UNUSED_7",
    "I32",
    "UNUSED_9",
    "I64",
    "BINARY",
    "STRUCT",
    "MAP",
    "SET",
    "LIST",
    "UUID",
};

static char* hf_ft[] = {
    "FT_NONE",  // generic
    "FT_STOP",  // stop
    "FT_VOID",  // void
    "FT_BOOLEAN",
    "FT_INT8",
    "FT_DOUBLE",
    "FT_UNUSED_5",
    "FT_INT16",
    "FT_UNUSED_7",
    "FT_INT32",
    "FT_UNUSED_9",
    "FT_INT64",
    "FT_BYTES", // or FT_STRING for alt_type
    "FT_NONE",  // struct
    "FT_NONE",  // map
    "FT_NONE",  // set
    "FT_NONE",  // list
    "FT_GUID",
};

static char* hf_base[] = {
    "BASE_NONE", // generic
    "BASE_STOP", // stop
    "BASE_VOID", // void
    "BASE_NONE", // bool
    "BASE_DEC",  // i8
    "BASE_NONE", // double
    "BASE_NONE", // unused_5
    "BASE_DEC",  // i16
    "BASE_NONE", // unused_7
    "BASE_DEC",  // i32
    "BASE_NONE", // unused_9
    "BASE_DEC",  // i64
    "BASE_NONE", // binary
    "BASE_NONE", // struct
    "BASE_NONE", // map
    "BASE_NONE", // set
    "BASE_NONE", // list
    "BASE_NONE", // uuid
};

static char* mandatory_to_opt[] = {
    "TRUE",
    "FALSE",
};

static const char* hf = "hf";
static const char* ett = "ett";

typedef enum _kind_t {
    // TODO: Revoir l’utilisation exacte pour éviter les confusions…
    NO_CONTEXT,
    ENUM,
    TYPEDEF,
    CONTAINER,
    FIELD,
    STRUCT,
    EXCEPTION,
    RESULT,
    PARAMETER,
    ONEWAY,
    CALL,
} kind_t;

typedef struct _context_t {
    FILE* stream;
    const tip_types_t* const types;
    const proto_t* const proto;
    const char* owner;
    kind_t kind;
    bool debug;
} context_t;

static void
write_title_case(FILE* stream, const char* string)
{
    const char SPACE = ' ';
    char* title = g_strdup(string);
    char* next = title;
    // Replace all '_' with a space and make them follow with an uppercase.
    while (next != NULL) {
        *next = g_ascii_toupper(*next);
        next = strchr(next, UNDERSCORE);
        if (next != NULL) {
            *next++ = SPACE;
        }
    }
    // Do the same with dots.
    next = title;
    while (next != NULL) {
        *next = g_ascii_toupper(*next);
        next = strchr(next, DOT);
        if (next != NULL) {
            *next++ = SPACE;
        }
    }
    fprintf(stream, "%s", title);
    g_free(title);
}

static void
write_member_api_close(FILE* stream, api_version_t target_api)
{
    if (target_api >= API_4_4) {
        fprintf(stream, ", NULL");
    }
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                 Header and beginning of the file                          //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
static void
write_cartouche(context_t* context)
{
    time_t now = time(NULL);
    struct tm * timeinfo = localtime(&now);
    fprintf(context->stream, "/* packet-%s.c\n", context->proto->abbrev);
    fprintf(context->stream, " * Routines for %s Protocol dissection.\n", context->proto->name);
    fprintf(context->stream, " * Based on work by Anders Broman <anders.broman[at]ericsson.com>\n");
    fprintf(context->stream, " *              and Triton Circonflexe <triton[at]kumal.info>\n");
    fprintf(context->stream, " *\n");
    fprintf(context->stream, " * Copyright %d, %s <%s>\n", 1900 + timeinfo->tm_year, context->proto->author, context->proto->email);
    fprintf(context->stream, " *\n");
    fprintf(context->stream, " * Wireshark - Network traffic analyzer\n");
    fprintf(context->stream, " * By Gerald Combs <gerald@wireshark.org>\n");
    fprintf(context->stream, " * Copyright 1998 Gerald Combs\n");
    fprintf(context->stream, " *\n");
    fprintf(context->stream, " * SPDX-License-Identifier: %s\n", context->proto->spdx);
    fprintf(context->stream, " */\n");
    fprintf(context->stream, "\n");
}

static void
write_includes(context_t* context)
{
    fprintf(context->stream, "#include <epan/packet.h>\n");
    if (context->proto->target_api < API_4_2 && context->proto->use_expert) {
        fprintf(context->stream, "#include <epan/expert.h>\n");
    }
    if (context->proto->plugin) {
        fprintf(context->stream, "#include <epan/dissectors/packet-thrift.h>\n");
    } else {
        fprintf(context->stream, "#include \"packet-thrift.h\"\n");
    }
    fprintf(context->stream, "\n");
}

static void
write_proto_init(context_t* context, bool expert_info)
{
    if (context->proto->target_api < API_4_4) {
        if (expert_info) {
            fprintf(context->stream, " = EI_INIT;\n", context->proto->abbrev);
        } else {
            fprintf(context->stream, " = -1;\n", context->proto->abbrev);
        }
    } else {
        fprintf(context->stream, ";\n", context->proto->abbrev);
    }
}

static void
write_declarations(context_t* context)
{
    fprintf(context->stream, "void proto_register_%s(void);\n", context->proto->abbrev);
    fprintf(context->stream, "void proto_reg_handoff_%s(void);\n", context->proto->abbrev);
    fprintf(context->stream, "\n");
    fprintf(context->stream, "/* Return codes or assimilated. */\n");
    fprintf(context->stream, "#define NOT_AN_EXPECTED_PDU  (0)\n");
    fprintf(context->stream, "\n");
    if (context->types->has_string) {
        fprintf(context->stream, "#define TMUTF8 NULL, { .encoding = ENC_UTF_8 }");
        write_member_api_close(context->stream, context->proto->target_api);
        fprintf(context->stream, "\n");
    }
    if (context->types->has_binary) {
        fprintf(context->stream, "#define TMRAW NULL, { .encoding = ENC_NA }");
        write_member_api_close(context->stream, context->proto->target_api);
        fprintf(context->stream, "\n");
    }
    fprintf(context->stream, "\n");
    if (context->types->has_union || context->types->has_exception) {
        fprintf(context->stream, "static const int DISABLE_SUBTREE = -1;\n");
        fprintf(context->stream, "\n");
    }
    fprintf(context->stream, "static int proto_%s", context->proto->abbrev);
    write_proto_init(context, false);
    fprintf(context->stream, "\n");
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                 Helpers used in multiple places                           //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
static prefix_t
get_prefix(const context_t* context, const hf_data_t* hf_data, prefix_t prefix)
{
    bool prevent_duplicate = true;
    if (!prevent_duplicate) {
        return prefix;
    }
    switch (prefix) {
    case FILENAME:
        if (hf_data->file_name != NULL && strcmp(context->proto->abbrev, hf_data->file_name) == 0) {
            return NO_PREFIX;
        }
        return FILENAME;
    case NAMESPACE:
        if (hf_data->name_space != NULL && strcmp(context->proto->abbrev, hf_data->name_space) == 0) {
            return NO_PREFIX;
        }
        return NAMESPACE;
    default:
        return prefix;
    }
}

static void
fprint_hf_name(context_t* context, const char* owner, const hf_data_t* const hf_data, separator_t sep)
{
    assert(hf_data != NULL);
    assert(hf_data->identifier != NULL);
    assert(context != NULL);
    bool apply_prefix = context->kind < RESULT || context->proto->prefix_method;
    prefix_t prefix_type = apply_prefix ? context->proto->prefix_type : NO_PREFIX;
    const char* prefix = NULL;
    prefix_type = get_prefix(context, hf_data, prefix_type);
    switch (prefix_type) {
    case FILENAME:
        prefix = hf_data->file_name;
        break;
    case NAMESPACE:
        prefix = hf_data->name_space;
        break;
    case NO_PREFIX:
        break;
    default:
        assert(false);
        break;
    }
    fprintf(context->stream, "%s%c", context->proto->abbrev, sep);
    if (prefix != NULL) {
        fprintf(context->stream, "%s%c", prefix, sep);
    }
    if (owner != NULL && (prefix == NULL || strcmp(prefix, owner))) {
        fprintf(context->stream, "%s%c", owner, sep);
    }
    fprintf(context->stream, "%s", hf_data->identifier);
}

static void
write_id_declaration(context_t* context, const char* id_type, hf_data_t* hf_data)
{
    fprintf(context->stream, "static int %s_", id_type);
    fprint_hf_name(context, context->owner, hf_data, UNDERSCORE);
    write_proto_init(context, false);
}

static void
write_content_t_member_contained(context_t* context, tip_field_t* field)
{
    assert(context != NULL);
    assert(field != NULL);
    if (field->hf_data.target_type != NULL) { // container<named_type>
        fprint_hf_name(context, NULL, field->hf_data.target_type, UNDERSCORE);
        fprintf(context->stream, "_%s", member);
    } else if (context->kind == FIELD) { // container<basic_type> in structure definition.
        fprint_hf_name(context, context->owner, &(field->hf_data), UNDERSCORE);
    } else if (context->kind == CONTAINER) { // container<basic_type> everywhere else (parameters, return types…)
        fprintf(context->stream, "%s", field->hf_data.identifier);
    } else {
        fprint_hf_name(context, NULL, &(field->hf_data), UNDERSCORE);
    }
}

static void
write_member_t_member_contained(context_t* context, hf_data_t* hf_data, int16_t index, bool mandatory, thrift_type_enum_t type, bool alt_type, tip_field_t* key, tip_field_t* value)
{
    assert(context != NULL);
    assert(hf_data != NULL);
    context_t local_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = NULL,
        .kind = CONTAINER,
        .debug = context->debug,
    };
    hf_data_t* target_type = hf_data->target_type != NULL ? hf_data->target_type : hf_data;
    switch (context->kind) {
    case STRUCT:
        local_ctx.owner = hf_data->identifier;
        local_ctx.kind = FIELD;
        break;
    case FIELD:
        local_ctx.owner = context->owner;
        local_ctx.kind = FIELD;
        break;
    case PARAMETER:
        break;
    default:
        // TODO: assert(false);
        break;
    }
    if (type == DE_THRIFT_T_VOID) {
        fprintf(context->stream, "{ NULL");
    } else if (alt_type && type == DE_THRIFT_T_STRUCT) {
        fprintf(context->stream, "{ &DISABLE_SUBTREE");
    } else {
        fprintf(context->stream, "{ &hf_");
        fprint_hf_name(context, context->owner, hf_data, UNDERSCORE);
    }
    fprintf(context->stream, ", %d, %s, DE_THRIFT_T_%s, ", index, mandatory_to_opt[mandatory], uppercase[1+type]);
    switch (type) {
    case DE_THRIFT_T_MAP:
        fprintf(context->stream, "&ett_");
        fprint_hf_name(context, context->owner, hf_data, UNDERSCORE);
        fprintf(context->stream, ", { .m.key = &");
        write_content_t_member_contained(&local_ctx, key);
        fprintf(context->stream, ", .m.value = &");
        write_content_t_member_contained(&local_ctx, value);
        fprintf(context->stream, " }");
        write_member_api_close(context->stream, context->proto->target_api);
        fprintf(context->stream, " }");
        break;
    case DE_THRIFT_T_SET:
    case DE_THRIFT_T_LIST:
        fprintf(context->stream, "&ett_");
        fprint_hf_name(context, context->owner, hf_data, UNDERSCORE);
        fprintf(context->stream, ", { .element = &");
        write_content_t_member_contained(&local_ctx, value);
        fprintf(context->stream, " }");
        write_member_api_close(context->stream, context->proto->target_api);
        fprintf(context->stream, " }");
        break;
    case DE_THRIFT_T_STRUCT:
        local_ctx.kind = STRUCT;
        if (alt_type) {
            fprintf(context->stream, "&DISABLE_SUBTREE");
        } else {
            fprintf(context->stream, "&ett_");
            fprint_hf_name(&local_ctx, NULL, target_type, UNDERSCORE);
        }
        fprintf(context->stream, ", { .s.members = ");
        fprint_hf_name(&local_ctx, NULL, target_type, UNDERSCORE);
        fprintf(context->stream, ", .s.expert_info = NULL }");
        write_member_api_close(context->stream, context->proto->target_api);
        fprintf(context->stream, " }");
        break;
    case DE_THRIFT_T_BINARY:
        if (alt_type) {
            fprintf(context->stream, "TMUTF8 }");
        } else {
            fprintf(context->stream, "TMRAW }");
        }
        break;
    default:
        fprintf(context->stream, "TMFILL }");
        break;
    }
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                 Definition of all the hf_ids                              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
static void
write_hf_id_init_container(context_t* context, tip_field_t* field)
{
    assert(field != NULL);
    assert(context != NULL);
    assert(field->_is_set.type);
    hf_data_t* hf_data = &(field->hf_data);
    switch (field->type) {
    case DE_THRIFT_T_LIST:
    case DE_THRIFT_T_SET:
        write_hf_id_init_container(context, field->value);
        break;
    case DE_THRIFT_T_MAP:
        write_hf_id_init_container(context, field->key);
        write_hf_id_init_container(context, field->value);
        break;
    default:
        // TODO: assert(false);
        break;
    }
    if (hf_data->target_type == NULL) {
        write_id_declaration(context, hf, hf_data);
    }
}

static void
write_hf_id_init(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    hf_data_t* hf_data = value;
    if (!hf_data->is_used) {
        return;
    }
    if (!hf_data->is_contained && context->kind <= TYPEDEF) {
        return;
    }
    switch (context->kind) {
        tip_field_t* field;
    case FIELD:
    case PARAMETER:
    case RESULT:
        field = value;
        if (field->_is_set.type) {
            switch (field->type) {
            case DE_THRIFT_T_LIST:
            case DE_THRIFT_T_SET:
            case DE_THRIFT_T_MAP:
                write_hf_id_init_container(context, field);
                return;
            case DE_THRIFT_T_STRUCT:
                if (field->_is_set.alt_type) {
                    return;
                }
                break;
            default:
                // TODO: assert(false);
                break;
            }
        }
        break;
    default:
        // TODO: assert(false);
        break;
    }
    write_id_declaration(context, hf, hf_data);
}

static void
write_hf_id_struct(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    assert(context->kind == STRUCT || context->kind == EXCEPTION);
    tip_struct_t* s = value;
    if (!s->hf_data.is_used) {
        return;
    }
    // First all its fields.
    context_t field_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = s->hf_data.identifier,
        .kind = FIELD,
        .debug = context->debug,
    };
    g_slist_foreach(s->fields, write_hf_id_init, &field_ctx);
    if (!s->is_union && s->hf_data.is_contained || context->kind == EXCEPTION) {
        // Then the structure itself.
        write_hf_id_init(value, data);
    }
}

static void
write_hf_id_function(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    tip_function_t* f = value;
    context_t func_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = f->hf_data.identifier,
        .kind = PARAMETER,
        .debug = context->debug,
    };
    // The parameters…
    g_slist_foreach(f->params, write_hf_id_init, &func_ctx);
    // … and the return type.
    func_ctx.kind = RESULT;
    assert(f->result != NULL);
    assert(f->result->_is_set.type);
    if (f->result->type > DE_THRIFT_T_VOID) {
        write_hf_id_init(f->result, &func_ctx);
    }
}

static void
write_hf_ids(context_t* context)
{
    assert(context != NULL);
    context->kind = ENUM;
    g_slist_foreach(context->types->enumerations, write_hf_id_init, context);
    context->kind = TYPEDEF;
    g_slist_foreach(context->types->typedefs, write_hf_id_init, context);
    context->kind = STRUCT;
    g_slist_foreach(context->types->structures, write_hf_id_struct, context);
    context->kind = EXCEPTION;
    g_slist_foreach(context->types->exceptions, write_hf_id_struct, context);
    g_slist_foreach(context->types->functions, write_hf_id_function, context);
    fprintf(context->stream, "\n");
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                 Definition of the trees’ ett_id                           //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
static void
write_ett_id_init_container(context_t* context, tip_field_t* field)
{
    assert(field != NULL);
    assert(context != NULL);
    hf_data_t* hf_data = &(field->hf_data);
    switch (field->type) {
    case DE_THRIFT_T_LIST:
    case DE_THRIFT_T_SET:
        write_ett_id_init_container(context, field->value);
        write_id_declaration(context, ett, hf_data);
        break;
    case DE_THRIFT_T_MAP:
        write_ett_id_init_container(context, field->key);
        write_ett_id_init_container(context, field->value);
        write_id_declaration(context, ett, hf_data);
        break;
    default:
        // TODO: assert(false);
        break;
    }
}

static void
write_ett_id_init(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    hf_data_t* hf_data = value;
    if (!hf_data->is_used) {
        return;
    }
    if (!hf_data->is_contained && context->kind <= TYPEDEF) {
        return;
    }
    switch (context->kind) {
        tip_field_t* field;
    case FIELD:
    case PARAMETER:
    case RESULT:
        field = value;
        if (field->_is_set.type) {
            switch (field->type) {
            case DE_THRIFT_T_LIST:
            case DE_THRIFT_T_SET:
            case DE_THRIFT_T_MAP:
                write_ett_id_init_container(context, field);
                break;
            default:
                // TODO: assert(false);
                break;
            }
        }
        break;
    case STRUCT:
    case EXCEPTION:
        write_id_declaration(context, "ett", hf_data);
        break;
    default:
        // TODO: assert(false);
        break;
    }
}

static void
write_ett_id_struct(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    assert(context->kind == STRUCT || context->kind == EXCEPTION);
    tip_struct_t* s = value;
    if (!s->hf_data.is_used) {
        return;
    }
    // First all its fields.
    context_t field_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = s->hf_data.identifier,
        .kind = FIELD,
        .debug = context->debug,
    };
    g_slist_foreach(s->fields, write_ett_id_init, &field_ctx);
    if (!s->is_union) {
        // Then the structure itself.
        write_ett_id_init(value, data);
    }
}

static void
write_ett_id_function(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    tip_function_t* f = value;
    context_t func_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = f->hf_data.identifier,
        .kind = PARAMETER,
        .debug = context->debug,
    };
    // The parameters…
    g_slist_foreach(f->params, write_ett_id_init, &func_ctx);
    // … and the return type.
    func_ctx.kind = RESULT;
    assert(f->result->_is_set.type);
    write_ett_id_init(f->result, &func_ctx);
}

static void
write_ett_ids(context_t* context)
{
    assert(context != NULL);
    fprintf(context->stream, "static int ett_%s", context->proto->abbrev);
    write_proto_init(context, false);
    context->kind = STRUCT;
    g_slist_foreach(context->types->structures, write_ett_id_struct, context);
    context->kind = EXCEPTION;
    g_slist_foreach(context->types->exceptions, write_ett_id_struct, context);
    g_slist_foreach(context->types->functions, write_ett_id_function, context);
    fprintf(context->stream, "\n");
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                 Definition of the exception expert field infos            //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
static void
write_expert_field_init(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    hf_data_t* hf_data = value;
    if (!hf_data->is_used) {
        return;
    }
    fprintf(context->stream, "static expert_field ei_");
    fprint_hf_name(context, context->owner, hf_data, UNDERSCORE);
    write_proto_init(context, true);
}

static void
write_expert_fields(context_t* context)
{
    if (context->types->has_exception) {
        g_slist_foreach(context->types->exceptions, write_expert_field_init, context);
        fprintf(context->stream, "\n");
    }
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                 Declaration of the structures' array                      //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
static void
write_struct_decl_init(context_t* context, hf_data_t* hf_data)
{
    fprintf(context->stream, "static const thrift_member_t ");
    fprint_hf_name(context, NULL, hf_data, UNDERSCORE);
}

static void
write_struct_declaration(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    hf_data_t* hf_data = value;
    tip_struct_t* s = value;
    if (!hf_data->is_used) {
        return;
    }
    write_struct_decl_init(context, hf_data);
    fprintf(context->stream, "[%u];\n", g_slist_length(s->fields) + 1);
    // TODO: Improve logic & allow customization. For instance: filter_by_type_name
    if (hf_data->is_contained) {
        write_struct_decl_init(context, hf_data);
        fprintf(context->stream, "_%s;\n", member);
    }
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                 Definition of the enumerations                            //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
static void
write_enum_value(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    tip_const_value_t* enum_value = value;
    fprintf(context->stream, "    { %2d, \"", enum_value->int_value);
    write_title_case(context->stream, enum_value->hf_data.identifier);
    fprintf(context->stream, "\" },\n");
}

static void
write_enumeration(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    tip_enum_t* enum_type = value;
    hf_data_t* hf_data = value;
    if (!hf_data->is_used) {
        return;
    }
    fprintf(context->stream, "static const value_string ");
    // TODO: Improve logic & allow customization.
    fprint_hf_name(context, NULL, hf_data, UNDERSCORE);
    fprintf(context->stream, "_vals[] = {\n");
    g_slist_foreach(enum_type->values, write_enum_value, context);
    fprintf(context->stream, "    {  0, NULL }\n");
    fprintf(context->stream, "};\n");
    // TODO: Improve logic & allow customization. Allow in case of filter by type name (instead of only field name).
    if (hf_data->is_contained) {
        fprintf(context->stream, "static const thrift_member_t ");
        fprint_hf_name(context, NULL, hf_data, UNDERSCORE);
        fprintf(context->stream, "_%s = ", member);
        write_member_t_member_contained(context, hf_data, 0, true, DE_THRIFT_T_I32, true, NULL, NULL);
        fprintf(context->stream, ";\n");
    }
    fprintf(context->stream, "\n");
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                 Definition of the containers used in structures           //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
static void
write_containers(context_t* context)
{
    /*foreach(map) { // WARN: Only if not struct.
        // TODO: fprintf(context->stream, "static const thrift_member_t %s_key   = { &hf_%s,  0, FALSE, DE_THRIFT_T_%s, %s };\n", hf_name(map), hf_name(map key), uppercase(type), tm_helper(type));
        // TODO: fprintf(context->stream, "static const thrift_member_t %s_value = { &hf_%s,  0, FALSE, DE_THRIFT_T_%s, %s };\n", hf_name(map), hf_name(map val), uppercase(type), tm_helper(type));
        fprintf(context->stream, "\n");
    } // */
    /*foreach(list + set) { // WARN: Only if not struct.
        // TODO: fprintf(context->stream, "static const thrift_member_t %s_element = { &hf_%s, 0, FALSE, DE_THRIFT_T_%s, %s };\n", hf_name(list), hf_name(list elt), uppercase(type), tm_helper(type));
        fprintf(context->stream, "\n");
    } // */
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//      Definition of the thrift_member_t arrays for the structures          //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
static void
write_struct_container_init(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    tip_field_t* field = value;
    context_t local_context = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = context->owner,
        .kind = CONTAINER,
        .debug = context->debug,
    };
    switch (field->type) {
    case DE_THRIFT_T_MAP:
        write_struct_container_init(field->key, &local_context);
        write_struct_container_init(field->value, &local_context);
        break;
    case DE_THRIFT_T_SET:
    case DE_THRIFT_T_LIST:
        write_struct_container_init(field->value, &local_context);
        break;
    default:
        // TODO: assert(false);
        break;
    }
    if (context->kind == CONTAINER && field->hf_data.target_type == NULL) {
        fprintf(context->stream, "static const thrift_member_t ");
        fprint_hf_name(context, context->owner, &(field->hf_data), UNDERSCORE);
        fprintf(context->stream, " = ");
        write_member_t_member_contained(context, &(field->hf_data), field->index, field->mandatory, field->type, field->_is_set.alt_type, field->key, field->value);
        fprintf(context->stream, ";\n");
    }
}

static void
write_typedef_definition(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    tip_field_t* field = value;
    if (!field->hf_data.is_used || !field->hf_data.is_contained) {
        return;
    }
    fprintf(context->stream, "static const thrift_member_t ");
    fprint_hf_name(context, context->owner, &(field->hf_data), UNDERSCORE);
    fprintf(context->stream, "%c%s = ", UNDERSCORE, member);
    write_member_t_member_contained(context, value, 0, true, field->type, field->_is_set.alt_type, field->key, field->value);
    fprintf(context->stream, ";\n\n");
}

static void
write_field_definition(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    tip_field_t* field = value;
    fprintf(context->stream, "    ");
    write_member_t_member_contained(context, &(field->hf_data), field->index, field->mandatory, field->type, field->_is_set.alt_type, field->key, field->value);
    fprintf(context->stream, ",\n");
}

static void
write_struct_definition(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    tip_struct_t* s = value;
    context_t local_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = s->hf_data.identifier,
        .kind = FIELD,
        .debug = context->debug,
    };
    if (!s->hf_data.is_used) {
        return;
    }
    g_slist_foreach(s->fields, write_struct_container_init, &local_ctx);
    write_struct_decl_init(context, value);
    fprintf(context->stream, "[] = {\n");
    g_slist_foreach(s->fields, write_field_definition, &local_ctx);
    fprintf(context->stream, "    { NULL, 0, FALSE, DE_THRIFT_T_STOP, TMFILL }\n");
    fprintf(context->stream, "};\n");
    // TODO: Improve logic & allow customization. For instance: filter_by_type_name
    if (s->hf_data.is_contained) {
        write_struct_decl_init(context, value);
        fprintf(context->stream, "_%s = ", member);
        write_member_t_member_contained(context, value, 0, true, DE_THRIFT_T_STRUCT, s->is_union, NULL, NULL);
        fprintf(context->stream, ";\n");
    }
    fprintf(context->stream, "\n");
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                 Helper to better handle failures to dissect               //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
static void
write_proto_set_end(context_t* context)
{
    fprintf(context->stream, "static void\n");
    fprintf(context->stream, "%s_proto_set_end(proto_item *pi, tvbuff_t *tvb, int offset, thrift_option_data_t *thrift_opt, int max_depth)\n", context->proto->abbrev);
    fprintf(context->stream, "{\n");
    fprintf(context->stream, "    if (offset > 0) {\n");
    fprintf(context->stream, "        proto_item_set_end(pi, tvb, offset);\n");
    fprintf(context->stream, "    } else if (max_depth > 0) {\n");
    fprintf(context->stream, "        thrift_opt->nested_type_depth = max_depth;\n");
    fprintf(context->stream, "    }\n");
    fprintf(context->stream, "}\n");
    fprintf(context->stream, "\n");
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                 Dissection of a specific Thrift command                   //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
static void
write_member_contained(context_t* context, tip_field_t* field)
{
    assert(context != NULL);
    assert(field != NULL);
    assert(field->_is_set.type);
    context_t contain_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = context->owner,
        .kind = CONTAINER,
        .debug = context->debug,
    };
    switch (field->type) {
    case DE_THRIFT_T_MAP:
        write_member_contained(&contain_ctx, field->key);
        write_member_contained(&contain_ctx, field->value);
        break;
    case DE_THRIFT_T_SET:
    case DE_THRIFT_T_LIST:
        write_member_contained(&contain_ctx, field->value);
        break;
    default:
        // TODO: assert(false);
        break;
    }
    if (context->kind == CONTAINER && field->hf_data.target_type == NULL) {
        fprintf(context->stream, "    const thrift_member_t %s = ", field->hf_data.identifier);
        write_member_t_member_contained(context, &(field->hf_data), field->index, field->mandatory, field->type, field->_is_set.alt_type, field->key, field->value);
        fprintf(context->stream, ";\n");
    }
}

static void
write_return_member(context_t* context, tip_field_t* field)
{
    if (field->type == DE_THRIFT_T_VOID) {
        // TODO: Without it, we need to alter the code generation below.
        //return;
    }
    fprintf(context->stream, "        ");
    write_member_t_member_contained(context, &(field->hf_data), field->index, field->mandatory, field->type, field->_is_set.alt_type, field->key, field->value);
    fprintf(context->stream, ",\n");
}

static void
write_exception_member(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    tip_field_t* field = value;
    context_t except_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = NULL,
        .kind = EXCEPTION,
        .debug = context->debug,
    };
    fprintf(context->stream, "        { &hf_");
    fprint_hf_name(&except_ctx, NULL, field->hf_data.target_type, UNDERSCORE);
    fprintf(context->stream, ", %d, TRUE, DE_THRIFT_T_STRUCT, &ett_", field->index);
    fprint_hf_name(&except_ctx, NULL, field->hf_data.target_type, UNDERSCORE);
    fprintf(context->stream, ", { .s.members = ");
    fprint_hf_name(&except_ctx, NULL, field->hf_data.target_type, UNDERSCORE);
    fprintf(context->stream, ", .s.expert_info = &ei_");
    fprint_hf_name(&except_ctx, NULL, field->hf_data.target_type, UNDERSCORE);
    fprintf(context->stream, " }");
    write_member_api_close(context->stream, context->proto->target_api);
    fprintf(context->stream, " },\n");
}

static void
write_parameter_contained(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    tip_field_t* field = value;
    write_member_contained(context, field);
}

static void
write_command_tree(context_t* context)
{
    fprintf(context->stream, "    proto_item *%s_pi = proto_tree_add_item(tree, proto_%s, tvb, offset, -1, ENC_NA);\n", context->proto->abbrev, context->proto->abbrev);
    fprintf(context->stream, "    proto_tree *%s_tree = proto_item_add_subtree(%s_pi, ett_%s);\n", context->proto->abbrev, context->proto->abbrev, context->proto->abbrev);
    fprintf(context->stream, "\n");
}

static void
write_parameter_dissect_open_common(context_t* context, tip_field_t* field)
{
    fprintf(context->stream, "    offset = dissect_thrift_t_%s(tvb, pinfo, %s_tree, offset, thrift_opt, TRUE, %d, hf_", lowercase[1+field->type], context->proto->abbrev, field->index);
    fprint_hf_name(context, context->owner, &(field->hf_data), UNDERSCORE);
}

static void
write_parameter_dissect(void* value, void* data)
{
    tip_field_t* field = value;
    context_t* context = data;
    if (field->type <= DE_THRIFT_T_VOID) {
        return;
    }
    switch (context->kind) {
    case CALL:
    case RESULT:
        fprintf(context->stream, "    ");
        break;
    case EXCEPTION:
        fprintf(context->stream, "        ");
        break;
    default:
        // TODO: assert(false);
        break;
    }
    context_t local_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = NULL,
        .kind = CONTAINER,
        .debug = context->debug,
    };
    switch (field->type) {
        tip_struct_t* s;
    case DE_THRIFT_T_STRUCT:
        s = (tip_struct_t*)field->hf_data.target_type;
        local_ctx.kind = (context->kind != EXCEPTION) ? STRUCT : EXCEPTION;
        if (context->kind == EXCEPTION) {
            fprintf(context->stream, "    offset = dissect_thrift_t_%s(tvb, pinfo, %s_tree, offset, thrift_opt, TRUE, %d, hf_", lowercase[1+field->type], context->proto->abbrev, field->index);
            fprint_hf_name(context, NULL, &(s->hf_data), UNDERSCORE);
            fprintf(context->stream, ", ett_");
            fprint_hf_name(&local_ctx, NULL, &(s->hf_data), UNDERSCORE);
        } else if (s->is_union) {
            fprintf(context->stream, "    offset = dissect_thrift_t_%s(tvb, pinfo, %s_tree, offset, thrift_opt, TRUE, %d, DISABLE_SUBTREE, DISABLE_SUBTREE", lowercase[1+field->type], context->proto->abbrev, field->index);
        } else {
            write_parameter_dissect_open_common(context, field);
            fprintf(context->stream, ", ett_");
            fprint_hf_name(&local_ctx, NULL, field->hf_data.target_type, UNDERSCORE);
        }
        fprintf(context->stream, ", ");
        fprint_hf_name(&local_ctx, NULL, field->hf_data.target_type, UNDERSCORE);
        break;
    case DE_THRIFT_T_BINARY:
        if (field->_is_set.alt_type) {
            fprintf(context->stream, "    offset = dissect_thrift_t_string(tvb, pinfo, %s_tree, offset, thrift_opt, TRUE, %d, hf_", context->proto->abbrev, field->index);
            fprint_hf_name(context, context->owner, value, UNDERSCORE);
        } else {
            write_parameter_dissect_open_common(context, field);
        }
        break;
    case DE_THRIFT_T_MAP:
        write_parameter_dissect_open_common(context, field);
        fprintf(context->stream, ", ett_");
        fprint_hf_name(context, context->owner, value, UNDERSCORE);
        fprintf(context->stream, ", &");
        write_content_t_member_contained(&local_ctx, field->key);
        fprintf(context->stream, ", &");
        write_content_t_member_contained(&local_ctx, field->value);
        break;
    case DE_THRIFT_T_SET:
    case DE_THRIFT_T_LIST:
        write_parameter_dissect_open_common(context, field);
        fprintf(context->stream, ", ett_");
        fprint_hf_name(context, context->owner, value, UNDERSCORE);
        fprintf(context->stream, ", &");
        write_content_t_member_contained(&local_ctx, field->value);
        break;
    default:
        write_parameter_dissect_open_common(context, field);
        break;
    }
    fprintf(context->stream, ");\n");
}

static void
write_oneway(context_t* context, const tip_function_t* const func)
{
    context_t local_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = func->hf_data.identifier,
        .kind = ONEWAY,
        .debug = context->debug,
    };
    if (context->proto->canary) {
        fprintf(context->stream, "    DISSECTOR_ASSERT(thrift_opt != NULL);\n");
        fprintf(context->stream, "    DISSECTOR_ASSERT(thrift_opt->canary == THRIFT_OPTION_DATA_CANARY);\n\n");
    }
    g_slist_foreach(func->params, write_parameter_contained, &local_ctx);
    write_command_tree(context);
    if (func->params != NULL) {
        g_slist_foreach(func->params, write_parameter_dissect, &local_ctx);
    } else {
        fprintf(context->stream, "    // Nothing to dissect here, except for the ending T_STOP.\n");
    }
    fprintf(context->stream, "\n    offset = dissect_thrift_t_stop(tvb, pinfo, %s_tree, offset);\n", context->proto->abbrev);
}

static void
write_call_reply(context_t* context, const tip_function_t* const func)
{
    context_t local_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = func->hf_data.identifier,
        .kind = CALL,
        .debug = context->debug,
    };
    write_member_contained(&local_ctx, func->result);
    g_slist_foreach(func->params, write_parameter_contained, &local_ctx);
    write_command_tree(context);
    if (context->proto->canary) {
        fprintf(context->stream, "    DISSECTOR_ASSERT(thrift_opt != NULL);\n");
        fprintf(context->stream, "    DISSECTOR_ASSERT(thrift_opt->canary == THRIFT_OPTION_DATA_CANARY);\n\n");
    }
    fprintf(context->stream, "    switch (thrift_opt->mtype) {\n");
    fprintf(context->stream, "    case ME_THRIFT_T_CALL:\n");
    g_slist_foreach(func->params, write_parameter_dissect, &local_ctx);
    fprintf(context->stream, "        break;\n");
    fprintf(context->stream, "    case ME_THRIFT_T_REPLY:\n");
    local_ctx.kind = RESULT;
    write_parameter_dissect(func->result, &local_ctx);
    fprintf(context->stream, "        break;\n");
    fprintf(context->stream, "    default:\n");
    fprintf(context->stream, "        return NOT_AN_EXPECTED_PDU;\n");
    fprintf(context->stream, "    }\n");
    fprintf(context->stream, "    offset = dissect_thrift_t_stop(tvb, pinfo, %s_tree, offset);\n", context->proto->abbrev);
}

static void
write_call_reply_except(context_t* context, const tip_function_t* const func)
{
    context_t local_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = func->hf_data.identifier,
        .kind = CALL,
        .debug = context->debug,
    };
    write_member_contained(&local_ctx, func->result);
    fprintf(context->stream, "    const thrift_member_t reply_with_exceptions[] = {\n");
    write_return_member(&local_ctx, func->result);
    g_slist_foreach(func->throws, write_exception_member, &local_ctx);
    fprintf(context->stream, "        { NULL, 0, FALSE, DE_THRIFT_T_STOP, TMFILL }\n");
    fprintf(context->stream, "    };\n");
    g_slist_foreach(func->params, write_parameter_contained, &local_ctx);
    write_command_tree(context);
    if (context->proto->canary) {
        fprintf(context->stream, "    DISSECTOR_ASSERT(thrift_opt != NULL);\n");
        fprintf(context->stream, "    DISSECTOR_ASSERT(thrift_opt->canary == THRIFT_OPTION_DATA_CANARY);\n\n");
    }
    fprintf(context->stream, "    switch (thrift_opt->mtype) {\n");
    fprintf(context->stream, "    case ME_THRIFT_T_CALL:\n");
    g_slist_foreach(func->params, write_parameter_dissect, &local_ctx);
    fprintf(context->stream, "        offset = dissect_thrift_t_stop(tvb, pinfo, %s_tree, offset);\n", context->proto->abbrev);
    fprintf(context->stream, "        break;\n");
    fprintf(context->stream, "    case ME_THRIFT_T_REPLY:\n");
    fprintf(context->stream, "        offset = dissect_thrift_t_struct(tvb, pinfo, tree, offset, thrift_opt, FALSE, 0, DISABLE_SUBTREE, DISABLE_SUBTREE, reply_with_exceptions);\n");
    fprintf(context->stream, "        break;\n");
    fprintf(context->stream, "    default:\n");
    fprintf(context->stream, "        return NOT_AN_EXPECTED_PDU;\n");
    fprintf(context->stream, "    }\n");
}

static void
write_exception_case(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    tip_field_t* field = value;
    fprintf(context->stream, "        case %d:\n", field->index);
    write_parameter_dissect(field, context);
    fprintf(context->stream, "            break;\n");
}

static void
write_call_reply_except_switch(context_t* context, const tip_function_t* const func)
{
    context_t local_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = func->hf_data.identifier,
        .kind = CALL,
        .debug = context->debug,
    };
    write_member_contained(&local_ctx, func->result);
    g_slist_foreach(func->params, write_parameter_contained, &local_ctx);
    write_command_tree(context);
    if (context->proto->canary) {
        fprintf(context->stream, "    DISSECTOR_ASSERT(thrift_opt != NULL);\n");
        fprintf(context->stream, "    DISSECTOR_ASSERT(thrift_opt->canary == THRIFT_OPTION_DATA_CANARY);\n\n");
    }
    fprintf(context->stream, "    switch (thrift_opt->mtype) {\n");
    fprintf(context->stream, "    case ME_THRIFT_T_CALL:\n");
    g_slist_foreach(func->params, write_parameter_dissect, &local_ctx);
    fprintf(context->stream, "        break;\n");
    fprintf(context->stream, "    case ME_THRIFT_T_REPLY:\n");
    fprintf(context->stream, "        switch (thrift_opt->reply_field_id) {\n");
    fprintf(context->stream, "        case 0:\n    ");
    local_ctx.kind = RESULT;
    write_parameter_dissect(func->result, &local_ctx);
    fprintf(context->stream, "            break;\n");
    local_ctx.kind = EXCEPTION;
    g_slist_foreach(func->throws, write_exception_case, &local_ctx);
    fprintf(context->stream, "        default:\n");
    fprintf(context->stream, "            return NOT_AN_EXPECTED_PDU;\n");
    fprintf(context->stream, "        }\n");
    fprintf(context->stream, "        break;\n");
    fprintf(context->stream, "    default:\n");
    fprintf(context->stream, "        return NOT_AN_EXPECTED_PDU;\n");
    fprintf(context->stream, "    }\n");
    fprintf(context->stream, "\n    offset = dissect_thrift_t_stop(tvb, pinfo, %s_tree, offset);\n", context->proto->abbrev);
}

static void
write_function(void* func, void* ctx)
{
    assert(func != NULL);
    assert(ctx != NULL);
    const tip_function_t* cmd = func;
    context_t* context = ctx;
    assert(cmd->hf_data.identifier != NULL);
    bool oneway = cmd->result->type == DE_THRIFT_T_VOID && cmd->result->_is_set.alt_type;
    bool throws = cmd->throws != NULL;
    bool opt_not_used = oneway && cmd->params == NULL && !context->proto->canary;
    fprintf(context->stream, "static int\n");
    fprintf(context->stream, "dissect_");
    fprint_hf_name(context, context->owner, &(cmd->hf_data), UNDERSCORE);
    fprintf(context->stream, "(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void *data");
    if (opt_not_used) {
        fprintf(context->stream, " _U_");
    }
    fprintf(context->stream, ")\n{\n");
    fprintf(context->stream, "    thrift_option_data_t *thrift_opt = (thrift_option_data_t *)data;\n");
    fprintf(context->stream, "    int offset = 0;\n");
    if (oneway) {
        write_oneway(context, cmd);
    } else if (throws) {
        if (context->proto->exc_switch) {
            write_call_reply_except_switch(context, cmd);
        } else {
            write_call_reply_except(context, cmd);
        }
    } else {
        write_call_reply(context, cmd);
    }
    fprintf(context->stream, "\n");
    fprintf(context->stream, "    %s_proto_set_end(%s_pi, tvb, offset, thrift_opt, %u);\n", context->proto->abbrev, context->proto->abbrev, cmd->max_depth);
    // Batch/list<Span>/list<Log>/list<Tag>/basic_types = 8 levels.
    fprintf(context->stream, "    return offset;\n");
    fprintf(context->stream, "}\n");
    fprintf(context->stream, "\n");
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                 Main registration function                                //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

/*************************************/
/*  Register the hf_id of all types  */
/*************************************/
static void
write_hf_id_register_hf_data_open(context_t* context, hf_data_t* hf_data, char* type_name)
{
    assert(hf_data != NULL);
    hf_data_t hf_filter = {
        .file_name = hf_data->file_name,
        .name_space = hf_data->name_space,
        .identifier = (type_name != NULL) ? type_name : hf_data->identifier,
    };
    fprintf(context->stream, "        { &hf_");
    fprint_hf_name(context, context->owner, hf_data, UNDERSCORE);
    fprintf(context->stream, ",\n");
    fprintf(context->stream, "            { \"");
    write_title_case(context->stream, hf_filter.identifier);
    fprintf(context->stream, "\", \"");
    fprint_hf_name(context, context->owner, &hf_filter, DOT);
    fprintf(context->stream, "\",\n");
}

static void
write_hf_id_register_field_open(context_t* context, tip_field_t* field)
{
    hf_data_t* hf_data = &(field->hf_data);
    if (field->type_name != NULL &&
        (hf_data->target_type == NULL || context->kind == RESULT)) {
        write_hf_id_register_hf_data_open(context, hf_data, field->type_name);
    } else {
        write_hf_id_register_hf_data_open(context, hf_data, NULL);
    }
}

static void
write_hf_id_register_close(FILE* stream)
{
    fprintf(stream, "                0x0, NULL, HFILL }\n");
    fprintf(stream, "        },\n");
}

static void
write_hf_id_register_enum(context_t* context, hf_data_t* hf_data)
{
    context_t enum_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = context->owner,
        .kind = ENUM,
        .debug = context->debug,
    };
    write_hf_id_register_hf_data_open(context, hf_data, NULL);
    fprintf(context->stream, "                FT_INT32, BASE_DEC, VALS(");
    if (hf_data->target_type != NULL) {
        fprint_hf_name(&enum_ctx, NULL, hf_data->target_type, UNDERSCORE);
    } else {
        fprint_hf_name(&enum_ctx, context->owner, hf_data, UNDERSCORE);
    }
    fprintf(context->stream, "_vals),\n");
    write_hf_id_register_close(context->stream);
}

static void
write_hf_id_register_field(context_t* context, tip_field_t* field)
{
    assert(field->_is_set.type);
    if (field->_is_set.alt_type) {
        switch (field->type) {
        case DE_THRIFT_T_BINARY:
            write_hf_id_register_field_open(context, field);
            fprintf(context->stream, "                FT_STRING, BASE_NONE, NULL,\n");
            write_hf_id_register_close(context->stream);
            return;
        case DE_THRIFT_T_I32:
            write_hf_id_register_enum(context, &(field->hf_data));
            return;
        case DE_THRIFT_T_STRUCT:
        case DE_THRIFT_T_VOID:
            return;
        default:
            assert(FALSE);
            break;
        }
    }
    if (field->type == DE_THRIFT_T_VOID) {
        return; // alt_type is only set for oneway functions.
    }
    write_hf_id_register_field_open(context, field);
    fprintf(context->stream, "                %s, %s, NULL,\n", hf_ft[1+field->type], hf_base[1+field->type]);
    write_hf_id_register_close(context->stream);
}

static void
write_hf_id_reg_container(context_t* context, tip_field_t* field)
{
    context_t contain_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = context->owner,
        .kind = CONTAINER,
        .debug = context->debug,
    };
    assert(field != NULL);
    assert(context != NULL);
    switch (field->type) {
    case DE_THRIFT_T_LIST:
    case DE_THRIFT_T_SET:
        write_hf_id_reg_container(&contain_ctx, field->value);
        break;
    case DE_THRIFT_T_MAP:
        write_hf_id_reg_container(&contain_ctx, field->key);
        write_hf_id_reg_container(&contain_ctx, field->value);
        break;
    default:
        // TODO: assert(false);
        break;
    }
    if (field->hf_data.target_type == NULL || context->kind != CONTAINER) {
        write_hf_id_register_field(context, field);
    }
}

static void
write_hf_id_reg(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    hf_data_t* hf_data = value;
    if (!hf_data->is_used) {
        return;
    }
    if (!hf_data->is_contained && context->kind <= TYPEDEF) {
        return;
    }
    switch (context->kind) {
    case FIELD:
    case PARAMETER:
    case RESULT:
    case TYPEDEF:
        write_hf_id_reg_container(context, value);
        break;
    case ENUM:
        write_hf_id_register_enum(context, hf_data);
        break;
    case STRUCT:
    case EXCEPTION:
        write_hf_id_register_hf_data_open(context, hf_data, NULL);
        fprintf(context->stream, "                %s, %s, NULL,\n", hf_ft[1+DE_THRIFT_T_STRUCT], hf_base[1+DE_THRIFT_T_STRUCT]);
        write_hf_id_register_close(context->stream);
        break;
    default:
        write_hf_id_register_hf_data_open(context, hf_data, NULL);
        write_hf_id_register_close(context->stream);
        break;
    }
}

static void
write_hf_id_reg_struct(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    assert(context->kind == STRUCT || context->kind == EXCEPTION);
    tip_struct_t* s = value;
    if (!s->hf_data.is_used) {
        return;
    }
    // First all its fields.
    context_t field_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = s->hf_data.identifier,
        .kind = FIELD,
        .debug = context->debug,
    };
    g_slist_foreach(s->fields, write_hf_id_reg, &field_ctx);
    if (!s->is_union && s->hf_data.is_contained || context->kind == EXCEPTION) {
        // Then the structure itself.
        write_hf_id_reg(value, data);
    }
}

static void
write_hf_id_reg_function(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    tip_function_t* f = value;
    context_t func_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = f->hf_data.identifier,
        .kind = PARAMETER,
        .debug = context->debug,
    };
    // The parameters…
    g_slist_foreach(f->params, write_hf_id_reg, &func_ctx);
    // … and the return type.
    func_ctx.kind = RESULT;
    if (f->result != NULL) {
        assert(f->result->_is_set.type);
        write_hf_id_reg(f->result, &func_ctx);
    }
}

static void
write_hf_register_info(context_t* context)
{
    assert(context != NULL);
    context->kind = ENUM;
    g_slist_foreach(context->types->enumerations, write_hf_id_reg, context);
    context->kind = TYPEDEF;
    g_slist_foreach(context->types->typedefs, write_hf_id_reg, context);
    context->kind = STRUCT;
    g_slist_foreach(context->types->structures, write_hf_id_reg_struct, context);
    context->kind = EXCEPTION;
    g_slist_foreach(context->types->exceptions, write_hf_id_reg_struct, context);
    g_slist_foreach(context->types->functions, write_hf_id_reg_function, context);
}

/*************************************/
/*  Register the trees' ett_id       */
/*************************************/
static void
write_ett_id_address(context_t* context, hf_data_t* hf_data)
{
    fprintf(context->stream, "        &ett_");
    fprint_hf_name(context, context->owner, hf_data, UNDERSCORE);
    fprintf(context->stream, ",\n");
}

static void
write_ett_id_reg_container(context_t* context, tip_field_t* field)
{
    assert(field != NULL);
    assert(context != NULL);
    hf_data_t* hf_data = &(field->hf_data);
    switch (field->type) {
    case DE_THRIFT_T_LIST:
    case DE_THRIFT_T_SET:
        write_ett_id_reg_container(context, field->value);
        write_ett_id_address(context, hf_data);
        break;
    case DE_THRIFT_T_MAP:
        write_ett_id_reg_container(context, field->key);
        write_ett_id_reg_container(context, field->value);
        write_ett_id_address(context, hf_data);
        break;
    default:
        // TODO: assert(false);
        break;
    }
}

static void
write_ett_id_register(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    hf_data_t* hf_data = value;
    if (!hf_data->is_used) {
        return;
    }
    if (!hf_data->is_contained && context->kind <= TYPEDEF) {
        return;
    }
    switch (context->kind) {
        tip_field_t* field;
    case FIELD:
    case PARAMETER:
    case RESULT:
        field = value;
        assert(field->_is_set.type);
        switch (field->type) {
        case DE_THRIFT_T_LIST:
        case DE_THRIFT_T_SET:
        case DE_THRIFT_T_MAP:
            write_ett_id_reg_container(context, field);
            break;
        default:
            // TODO: assert(false);
            break;
        }
        break;
    case STRUCT:
    case EXCEPTION:
        write_ett_id_address(context, hf_data);
        break;
    default:
        // TODO: assert(false);
        break;
    }
}

static void
write_ett_id_struct_register(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    assert(context->kind == STRUCT || context->kind == EXCEPTION);
    tip_struct_t* s = value;
    context_t local_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = s->hf_data.identifier,
        .kind = FIELD,
        .debug = context->debug,
    };
    if (!s->hf_data.is_used) {
        return;
    }
    // First all its fields.
    g_slist_foreach(s->fields, write_ett_id_register, &local_ctx);
    if (!s->is_union) {
        // Then the structure itself.
        write_ett_id_register(value, data);
    }
}

static void
write_ett_id_function_register(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    tip_function_t* f = value;
    context_t func_ctx = {
        .stream = context->stream,
        .types = context->types,
        .proto = context->proto,
        .owner = f->hf_data.identifier,
        .kind = PARAMETER,
        .debug = context->debug,
    };
    // The parameters…
    g_slist_foreach(f->params, write_ett_id_register, &func_ctx);
    // … and the return type.
    func_ctx.kind = RESULT;
    if (f->result != NULL) {
        assert(f->result->_is_set.type);
        write_ett_id_register(f->result, &func_ctx);
    }
}

/*************************************/
/*  Register the exceptions infos    */
/*************************************/
static void
write_ei_register_info(void* value, void* data)
{
    assert(value != NULL);
    assert(data != NULL);
    context_t* context = data;
    hf_data_t* hf_data = value;
    if (!hf_data->is_used) {
        return;
    }
    fprintf(context->stream, "        { &ei_");
    fprint_hf_name(context, context->owner, hf_data, UNDERSCORE);
    fprintf(context->stream, ", { \"");
    fprint_hf_name(context, context->owner, hf_data, DOT);
    fprintf(context->stream, "\", PI_RESPONSE_CODE, PI_NOTE, \"Server returned an exception (%s), request could not be processed,\", EXPFILL } },\n", hf_data->identifier);
}

/*************************************/
/*  Main registration function       */
/*************************************/
void
write_proto_register(context_t* context)
{
    bool use_expert = context->proto->use_expert;
    fprintf(context->stream, "void\n");
    fprintf(context->stream, "proto_register_%s(void)\n", context->proto->abbrev);
    fprintf(context->stream, "{\n");
    fprintf(context->stream, "    static hf_register_info hf[] = {\n");
    write_hf_register_info(context);
    fprintf(context->stream, "    };\n");
    fprintf(context->stream, "\n");
    fprintf(context->stream, "    /* setup protocol subtree arrays */\n");
    fprintf(context->stream, "    static gint* ett[] = {\n");
    fprintf(context->stream, "        &ett_%s,\n", context->proto->abbrev);
    fflush(context->stream);
    context->kind = STRUCT;
    g_slist_foreach(context->types->structures, write_ett_id_struct_register, context);
    fflush(context->stream);
    context->kind = EXCEPTION;
    g_slist_foreach(context->types->exceptions, write_ett_id_struct_register, context);
    fflush(context->stream);
    g_slist_foreach(context->types->functions, write_ett_id_function_register, context);
    fflush(context->stream);
    fprintf(context->stream, "    };\n");
    fprintf(context->stream, "\n");
    if (use_expert) {
        fprintf(context->stream, "    static ei_register_info ei[] = {\n");
        g_slist_foreach(context->types->exceptions, write_ei_register_info, context);
        fprintf(context->stream, "    };\n");
        fprintf(context->stream, "\n");
        fprintf(context->stream, "    expert_module_t* expert_%s;\n", context->proto->abbrev);
        fprintf(context->stream, "\n");
    }
    fprintf(context->stream, "    /* Register protocol name and description */\n");
    fprintf(context->stream, "    proto_%s = proto_register_protocol(\"%s Protocol\", \"%s\", \"%s\");\n", context->proto->abbrev, context->proto->name, context->proto->short_name, context->proto->abbrev);
    fprintf(context->stream, "\n");
    if (use_expert) {
        fprintf(context->stream, "    expert_%s = expert_register_protocol(proto_%s);\n", context->proto->abbrev, context->proto->abbrev);
        fprintf(context->stream, "\n");
    }
    fprintf(context->stream, "    /* register field array */\n");
    fprintf(context->stream, "    proto_register_field_array(proto_%s, hf, array_length(hf));\n", context->proto->abbrev);
    fprintf(context->stream, "\n");
    fprintf(context->stream, "    /* register subtree array */\n");
    fprintf(context->stream, "    proto_register_subtree_array(ett, array_length(ett));\n");
    if (use_expert) {
        fprintf(context->stream, "\n");
        fprintf(context->stream, "    expert_register_field_array(expert_%s, ei, array_length(ei));\n", context->proto->abbrev);
    }
    fprintf(context->stream, "}\n");
    fprintf(context->stream, "\n");
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                 Registration of the handoff                               //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
void
write_dissector_add_string(void* func, void* ctx)
{
    const tip_function_t* cmd = func;
    context_t* context = ctx;
    fprintf(context->stream, "    dissector_add_string(\"thrift.method_names\", \"%s\", create_dissector_handle(dissect_", cmd->hf_data.identifier);
    fprint_hf_name(context, context->owner, &(cmd->hf_data), UNDERSCORE);
    fprintf(context->stream, ", proto_%s));\n", context->proto->abbrev);
}

void
write_proto_reg_handoff(context_t* context)
{
    fprintf(context->stream, "void\n");
    fprintf(context->stream, "proto_reg_handoff_%s(void)\n", context->proto->abbrev);
    fprintf(context->stream, "{\n");
    g_slist_foreach(context->types->functions, write_dissector_add_string, context);
    fprintf(context->stream, "}\n");
    fprintf(context->stream, "\n");
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                 Modelines fixed footer                                    //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
void
write_modelines(context_t* context)
{
    fprintf(context->stream, "/*\n");
    fprintf(context->stream, " * Editor modelines  -  https://www.wireshark.org/tools/modelines.html\n");
    fprintf(context->stream, " *\n");
    fprintf(context->stream, " * Local variables:\n");
    fprintf(context->stream, " * c-basic-offset: 4\n");
    fprintf(context->stream, " * tab-width: 8\n");
    fprintf(context->stream, " * indent-tabs-mode: nil\n");
    fprintf(context->stream, " * End:\n");
    fprintf(context->stream, " *\n");
    fprintf(context->stream, " * vi: set shiftwidth=4 tabstop=8 expandtab:\n");
    fprintf(context->stream, " * :indentSize=4:tabSize=8:noTabs=true:\n");
    fprintf(context->stream, " */\n");
}

void
write_dissector(FILE* stream, const tip_types_t* const types, const proto_t* const proto)
{
    context_t context = {
        .stream = stream,
        .types = types,
        .proto = proto,
        .owner = NULL,
        .kind = NO_CONTEXT,
        .debug = true,
    };
    write_cartouche(&context);
    write_includes(&context);
    write_declarations(&context);
    write_hf_ids(&context);
    write_ett_ids(&context);
    if (proto->use_expert) {
        write_expert_fields(&context);
    }
    context.kind = STRUCT;
    g_slist_foreach(context.types->structures, write_struct_declaration, &context);
    context.kind = EXCEPTION;
    g_slist_foreach(context.types->exceptions, write_struct_declaration, &context);
    fprintf(context.stream, "\n");
    g_slist_foreach(context.types->enumerations, write_enumeration, &context);
    write_containers(&context);
    context.kind = TYPEDEF;
    g_slist_foreach(context.types->typedefs, write_typedef_definition, &context);
    context.kind = STRUCT;
    g_slist_foreach(context.types->structures, write_struct_definition, &context);
    context.kind = EXCEPTION;
    g_slist_foreach(context.types->exceptions, write_struct_definition, &context);
    write_proto_set_end(&context);
    g_slist_foreach(context.types->functions, write_function, &context);
    write_proto_register(&context);
    write_proto_reg_handoff(&context);
    write_modelines(&context);
}
