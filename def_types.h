#ifndef DEF_TYPES_H
#define DEF_TYPES_H
#include <glib.h>
#include <stdint.h>
#include <stdbool.h>

#include "packet-thrift.h"

typedef enum {
    ERROR_NONE,
    ERROR_SYNTAX,
    ERROR_GRAMMAR,
    ERROR_OUT_OF_RANGE,
    ERROR_ONEWAY_THROW,
    ERROR_UUID_NOT_SUPPORTED,
} tip_error_t;

typedef enum {
    CONTEXT_ROOT,
    CONTEXT_ENUM,
    CONTEXT_CONST,
    CONTEXT_UNION,
    CONTEXT_STRUCT,
    CONTEXT_EXCEPTION,
    CONTEXT_SERVICE,
    CONTEXT_FUNCTION,
    CONTEXT_THROWS,
} thrift_idl_context_t;

typedef enum {
    NS_SCOPE_OTHER,
    NS_SCOPE_ALL,
    NS_SCOPE_WIRESHARK,
} tip_namespace_scope_t;

typedef struct _thrift_idl_token_t {
    char* v; // Token string value;
    int line; // Line number of the token in the .thrift file.
} thrift_idl_token_t;

typedef struct _namespace_t {
    tip_namespace_scope_t scope;
    char*                 value;
} tip_namespace_t;

typedef struct _hf_data_t hf_data_t;
struct _hf_data_t {
    const char* file_name;
    const char* name_space;
    char* identifier;
    hf_data_t*  target_type;
    bool        is_used;
    bool        is_contained;
};

typedef struct _const_value_t {
    hf_data_t hf_data;
    bool implicit;
    int32_t  int_value;
} tip_const_value_t;

typedef struct _field_t tip_field_t;
struct _field_t {
    hf_data_t hf_data;
    bool mandatory; // always true in function parameters
                        // always false in unions
                        // always false in throws statements
    int16_t index;
    thrift_type_enum_t type;
    char* type_name;
    tip_field_t* key;   // Used only with map
    tip_field_t* value; // Used with map, list, and set
    uint32_t max_depth;  // Used with typedefs to avoid re-calculation.
    struct {
        bool index;
        bool type;
        bool alt_type; // Use alternate meaning for some types
                           // alt(DE_THRIFT_T_VOID) = oneway void
                           // alt(DE_THRIFT_T_BINARY) = string
                           // alt(DE_THRIFT_T_STRUCT) = union
    } _is_set;
};

typedef struct _enum_t {
    hf_data_t hf_data;
    GSList* values;     // tip_const_value_t*
} tip_enum_t;

typedef struct _struct_t {
    hf_data_t hf_data;
    GSList* fields;     // tip_field_t*
    uint32_t max_depth;
    bool is_union;
} tip_struct_t;

typedef struct _function_t {
    hf_data_t hf_data;
    GSList* params;     // tip_field_t*
    uint32_t max_depth;
    tip_field_t* result;
    GSList* throws;
} tip_function_t;

typedef struct _types_t {
    struct {
        GSList* common_strings;
        GHashTable* typedefs;
        GHashTable* enumerations;
        GHashTable* structures;
        GHashTable* exceptions;
    } l; // Locate elements.
    GSList* typedefs;
    GSList* enumerations;
    GSList* structures;
    GSList* exceptions;
    GSList* functions;
    bool has_string;
    bool has_binary;
    bool has_union;
    bool has_exception;
} tip_types_t;

typedef struct _thrift_idl_state_t {
    const char*          current_file_path; // /full/path/to/my_proto.thrift
    const char*          current_base_name; //               my_proto
    char*                current_namespace; // namespace wireshark ploproto;
    tip_namespace_t*     namespace_ws;
    tip_types_t*         types;
    thrift_idl_context_t context;
    thrift_idl_token_t*  token;
    GSList*              file_list; // char*
    void*                parser;
    void*                scanner;
    tip_error_t          grammar_error;
    bool                 lowercase;
    bool                 has_union;
    bool                 has_exception;
    bool                 supports_uuid;
} thrift_idl_state_t;

#endif
