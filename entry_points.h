#ifndef TRAVERSE_H
#define TRAVERSE_H

#include <stdio.h>
#include <glib.h>

#include "def_types.h"

typedef char separator_t;
static const separator_t UNDERSCORE = '_';
static const separator_t DOT = '.';

typedef enum _prefix_t {
    NO_PREFIX,
    FILENAME,
    NAMESPACE,
} prefix_t;

typedef enum _api_version_t {
    API_3_6, // First version covering every known types (as of Thrift 0.17.0).
    API_3_8, // Support for UUID (Thrift 0.18.0).
    API_4_2, // Support for expert info for exceptions.
    API_4_4, // Maybe support for raw dissectors.
} api_version_t;

typedef struct _proto_t {
    char* author;
    char* email;
    char* name;
    char* short_name;
    char* abbrev;
    char* year;
    char* spdx;
    char* return_filter;
    bool plugin;
    bool canary;
    bool lowercase;
    bool prefix_method;
    bool use_expert;
    bool exc_switch;
    int prefix_type;
    api_version_t target_api;
} proto_t;

typedef struct _annotation_data_t {
    proto_t*     proto;
    tip_types_t* types;
} annotation_data_t;

static char* de_types[] = {
    "GENERIC",
    "STOP",
    "VOID",
    "BOOL",
    "I8",
    "DOUBLE",
    "UNUSED_5",
    "I16",
    "UNUSED_7",
    "I32",
    "UNUSED_9",
    "I64",
    "BINARY",
    "STRUCT",
    "MAP",
    "SET",
    "LIST",
    "UUID",
};
static const char* return_id = "return";
static const char* map_key = "key";
static const char* map_value = "value";
static const char* ls_elt = "element";
static const char* member = "member";

extern char* de_types[];
extern const char* return_id;
extern const char* map_key;
extern const char* map_value;
extern const char* ls_elt;

/*! Add a file to the list of files to parse.
 *
 * @param[in] token The token containing the name of the file.
 * @param[in, out] state The state of the current parsing.
 */
void
tip_include_file(thrift_idl_token_t* token, thrift_idl_state_t* state);

/*! Set the common data used to build the hf_id from the current state.
 *
 * @param[in] state The current state that contains the file name and other useful data.
 * @param[in, out] hf_data The structure that need to contain all data.
 */
void
set_hf_data(const thrift_idl_state_t* const state, void* hf_data);

/*! Gets the basename of the given file, without the '.thrift' extension.
 *
 * @param[in] filepath The complete path to the file.
 * @param[in] lowercase Wether the file name must be converted to lowercase or not.
 */
char*
get_basename_thrift(const char* filepath, bool lowercase);

/*! Get the full name of a type, given the file it’s in and its name.
 *
 * @param[in] file The name of the file, without path or extension.
 * @param[in] name The name of the type inside the file.
 * @return The composite "file.name" full name for reference.
 */
char*
full_name(const char* file, const char* name);

/*! Traverse the structure to mark all used objects and their maximum depth.
 *
 * @param[in] function The Thrift function to traverse.
 * @param[in, out] data The complete protocol structure to find the types and parameters.
 */
void
annotate_used_depth(void* function, void* data);

void
free_token(thrift_idl_token_t* token);

void
final_cleanup(GSList* file_list, proto_t* proto, tip_types_t* types);

/*! Create the dissector source file to be integrated in Wireshark build.
 *
 * @param[in, out] stream The output stream in which the content of the file is to be written.
 * @param[in] types The complete annotated types description for the protocol.
 * @param[in] proto The protocol parameters.
 */
void
write_dissector(FILE* stream, const tip_types_t* const types, const proto_t* proto);
#endif
