#include <glib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "def_types.h"
#include "entry_points.h"

static FILE* debug;

typedef struct _work_t {
    char* basename;
    GHashTable* map;
} work_t;

char*
get_basename_thrift(const char* filepath, bool lowercase)
{
    char* basename = g_path_get_basename(filepath);
    if (lowercase) {
        char* temp = g_ascii_strdown(basename, -1);
        g_free(basename);
        basename = temp;
    }
    size_t full_length = strlen(basename);
    size_t length = full_length - 7;
    if (full_length > 7 && strncmp(&basename[length], ".thrift", 7) == 0) {
        char* no_ext = g_strndup(basename, length);
        g_free(basename);
        return no_ext;
    }
    return basename;
}

char*
full_name(const char* file, const char* name)
{
    return g_strdup_printf("%s.%s", file, name);
}

void
set_hf_data(const thrift_idl_state_t* const state, void* type)
{
    assert(state != NULL);
    assert(type != NULL);
    hf_data_t* hf_data = type;
    hf_data->file_name  = state->current_base_name;
    hf_data->name_space = state->current_namespace;
}
