    [1]  Document        ::=  namespace_t + list<definition_t>
    [2]  Header          ::=  namespace_t
    [3]  Include         ::=  nullptr_t + add to parse list
    [4]  CppInclude      ::=  nullptr_t
    [5]  Namespace       ::=  namespace_t = bool_t + string_t
    [6]  NamespaceScope  ::=  bool_t (all?)
    [7]  Definition      ::=  list<definition_t> (definition_t = union { const_t, typedef_t, enum_t, struct_t, service_t } + enum definition_type
    [8]  Const           ::=  const_t
    [9]  Typedef         ::=  string_t + string_t
    [10] Enum            ::=  'enum' Identifier '{' (Identifier ('=' IntConstant)? ListSeparator?)* '}'
    [12] Struct          ::=  list<field_t>
    [13] Union           ::=  list<field_t>
    [14] Exception       ::=  list<field_t>
    [15] Service         ::=  service_t = list<function_t> + string_t
    [16] Field           ::=  field_t
    [17] FieldID         ::=  const_t
    [18] FieldReq        ::=  bool_t (required?)
    [19] XsdFieldOptions ::=  nullptr_t
    [20] XsdAttrs        ::=  nullptr_t
    [21] Function        ::=  function_t
    [22] FunctionType    ::=  function_t = field_t + oneway?
    [23] Throws          ::=  list<field_t>
    [24] FieldType       ::=  field_t
    [25] DefinitionType  ::=  field_t
    [26] BaseType        ::=  thrift_type_enum_t
    [27] ContainerType   ::=  field_t
    [28] MapType         ::=  field_t
    [29] SetType         ::=  field_t
    [30] ListType        ::=  field_t
    [31] CppType         ::=  nullptr_t
    [32] ConstValue      ::=  const_t = union { int64_t, string_t } + token_t in { IDENTIFIER, LITERAL, INTEGER_CONST }
    [33] IntConstant     ::=  int64_t
    [34] DoubleConstant  ::=  nullptr_t
    [35] ConstList       ::=  nullptr_t
    [36] ConstMap        ::=  nullptr_t
    [37] Literal         ::=  string_t
    [38] Identifier      ::=  string_t


    [1]  Document        ::=  Header* Definition*
    [2]  Header          ::=  Namespace
    [3]  Include         ::=  Add to list; return nullptr_t
    [4]  CppInclude      ::=  nullptr_t
    [5]  Namespace       ::=  namespace_t
    [6]  NamespaceScope  ::=  namespace_scope_t
    [7]  Definition      ::=  Const | Typedef | Enum | Struct | Union | Exception | Service
    [8]  Const           ::=  'const' FieldType Identifier '=' ConstValue ListSeparator?
    [9]  Typedef         ::=  'typedef' DefinitionType Identifier
    [10] Enum            ::=  'enum' Identifier '{' (Identifier ('=' int16_t)? ListSeparator?)* '}'
    [12] Struct          ::=  'struct' Identifier 'xsd_all'? '{' Field* '}'
    [13] Union          ::=  'union' Identifier 'xsd_all'? '{' Field* '}'
    [14] Exception       ::=  'exception' Identifier '{' Field* '}'
    [15] Service         ::=  'service' Identifier ( 'extends' Identifier )? '{' Function* '}'
    [16] Field           ::=  FieldID? FieldReq? FieldType Identifier ('=' nullptr_t)? XsdFieldOptions ListSeparator?
    [17] FieldID         ::=  int16_t
    [18] FieldReq        ::=  'required' | 'optional' 
    [19] XsdFieldOptions ::=  'xsd_optional'? 'xsd_nillable'? XsdAttrs?
    [20] XsdAttrs        ::=  'xsd_attrs' '{' Field* '}'
    [21] Function        ::=  'oneway'? FunctionType Identifier '(' Field* ')' Throws? ListSeparator?
    [22] FunctionType    ::=  FieldType | 'void'
    [23] Throws          ::=  'throws' '(' Field* ')'
    [24] FieldType       ::=  field_t
    [25] DefinitionType  ::=  field_t
    [26] BaseType        ::=  thrift_type_enum_t
    [27] ContainerType   ::=  field_t
    [28] MapType         ::=  field_t
    [29] SetType         ::=  field_t
    [30] ListType        ::=  field_t
    [31] CppType         ::=  nullptr_t
    [32] ConstValue      ::=  const_value_t = int64_t | string_t | string_t | nullptr_t + enum
    [33] IntConstant     ::=  int64_t
    [34] DoubleConstant  ::=  nullptr_t
    [35] ConstList       ::=  nullptr_t
    [36] ConstMap        ::=  nullptr_t
    [37] Literal         ::=  token.string_t
    [38] Identifier      ::=  token.string_t
