/* packet-thrift.h
 *
 * Copyright 2015, Anders Broman <anders.broman[at]ericsson.com>
 * Copyright 2019-2021, Triton Circonflexe <triton[at]kumal.info>
 *
 * Wireshark - Network traffic analyzer
 * By Gerald Combs <gerald@wireshark.org>
 * Copyright 1998 Gerald Combs
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * Note: used by proprietary dissectors (too).
 */

#ifndef __PACKET_THRIFT_H__
#define __PACKET_THRIFT_H__

typedef enum {
    DE_THRIFT_T_GENERIC = -1, // Use this to delegate field dissection to generic dissector.
    DE_THRIFT_T_STOP,
    DE_THRIFT_T_VOID, // DE_THRIFT_T_UNUSED_1?
    DE_THRIFT_T_BOOL,
    DE_THRIFT_T_I8,
    DE_THRIFT_T_DOUBLE,
    DE_THRIFT_T_UNUSED_5, // Intended for U16?
    DE_THRIFT_T_I16,
    DE_THRIFT_T_UNUSED_7, // Intended for U32?
    DE_THRIFT_T_I32,
    DE_THRIFT_T_UNUSED_9, // Intended for U64?
    DE_THRIFT_T_I64,
    DE_THRIFT_T_BINARY,
    DE_THRIFT_T_STRUCT,
    DE_THRIFT_T_MAP,
    DE_THRIFT_T_SET,
    DE_THRIFT_T_LIST,
    DE_THRIFT_T_UUID,
} thrift_type_enum_t;

typedef enum {
    ME_THRIFT_T_CALL = 1,
    ME_THRIFT_T_REPLY,
    ME_THRIFT_T_EXCEPTION,
    ME_THRIFT_T_ONEWAY,
} thrift_method_type_enum_t;

/*
 * This is a list of flags even though not all combinations are available.
 * - Framed is compatible with everything;
 * - Default (0x00) is old binary;
 * - Binary can be augmented with Strict (message header is different but content is the same);
 * - Compact is incompatible with Binary & Strict as everything is coded differently;
 * - If Compact bit is set, Strict bit will be ignored (0x06 ~= 0x04).
 *
 * Valid values go from 0x00 (old binary format) to 0x05 (framed compact).
 *
 * Note: Compact is not fully supported yet.
 */
typedef enum {
    PROTO_THRIFT_BINARY = 0x00,
    PROTO_THRIFT_FRAMED = 0x01,
    PROTO_THRIFT_STRICT = 0x02,
    PROTO_THRIFT_COMPACT = 0x04
} thrift_protocol_enum_t;

#define THRIFT_OPTION_DATA_CANARY 0x8001da7a
#define THRIFT_REQUEST_REASSEMBLY       (-1)
#define THRIFT_SUBDISSECTOR_ERROR       (-2)

#define TMFILL NULL, { .m = { NULL, NULL } }

typedef struct _thrift_member_t thrift_member_t;
struct _thrift_member_t {
    const gint *p_hf_id;             /* The hf field for the struct member*/
    const gint16 fid;                /* The Thrift field id of the struct member*/
    const gboolean optional;         /* TRUE if element is optional, FALSE otherwise */
    const thrift_type_enum_t type;   /* The thrift type of the struct member */
    const gint *p_ett_id;            /* An ett field used for the subtree created if the member is a compound type. */
    union {
        const guint encoding;
        const thrift_member_t *element;
        const thrift_member_t *members;
        struct {
            const thrift_member_t *key;
            const thrift_member_t *value;
        } m;
    } u;
};

#endif /*__PACKET_THRIFT_H__ */
