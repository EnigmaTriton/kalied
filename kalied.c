#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <assert.h>
#include "def_types.h"
#include "thrift_idl_parser.h"
#include "thrift_idl_tokenizer_lex.h"
#include "entry_points.h"

void* ThriftIdlParserAlloc(void* (*allocProc)(size_t));
void ThriftIdlParser(void*, int, thrift_idl_token_t*, thrift_idl_state_t*);
void ThriftIdlParserFree(void*, void(*freeProc)(void*));
void ThriftIdlParserTrace(FILE *TraceFILE, char *zTracePrompt);

static FILE* debug_log = NULL;
static GSList* source_paths = NULL;
static const char* KALIED_author = "Kalied Generator";
static const char* KALIED_email = "kalied+thrift[at]kumal.info";
static const char* SPDX_GPL2_0_plus = "GPL-2.0-or-later";
static const char* default_return_filter = "result";

typedef struct _parameters_t {
    proto_t* proto;
    FILE* output;
    GSList* file_list;
} parameters_t;

static void
set_proto_parameters(tip_types_t* proto, const thrift_idl_state_t* const state)
{
    proto->has_union = state->has_union;
    proto->has_exception = state->has_exception;
}

static void
tip_clear_state(thrift_idl_state_t* state)
{
    if (state == NULL) {
        return;
    }
    state->grammar_error = ERROR_NONE;
    state->current_file_path = NULL;
    state->current_base_name = NULL;
    state->current_namespace = NULL;
    if (state->scanner != NULL) {
        thrift_idl_lex_destroy(state->scanner);
        state->scanner = NULL;
    }

    if (state->parser != NULL) {
        ThriftIdlParserFree(state->parser, g_free);
        state->parser = NULL;
    }
}

static void
tip_reinit_state(thrift_idl_state_t* state)
{
    if (state == NULL) {
        return;
    }
    tip_clear_state(state);
    state->parser = ThriftIdlParserAlloc(g_malloc);
}

/* Return owning data to free with g_free(). */
static char*
tip_canonicalize_absolute_filepath(const char* path) {
    int i, j;
    char* canon_path = g_new(char, strlen(path) + 1);
    /* replace all '\' to '/', and change '//' to '/' */
    for (i = 0, j = 0; path[i] != '\0'; i++) {
        if (path[i] == '\\' || path[i] == '/') {
            if (j > 0 && canon_path[j-1] == '/') {
                /* ignore redundant slash */
            } else {
                canon_path[j++] = '/';
            }
        } else {
#ifdef _WIN32
            canon_path[j++] = g_ascii_tolower(path[i]);
#else
            canon_path[j++] = path[i];
#endif
        }
    }
    canon_path[j] = '\0';

    if (g_path_is_absolute(canon_path)
        && g_file_test(canon_path, G_FILE_TEST_IS_REGULAR)
        && strstr(canon_path, "/../") == NULL) {
        return canon_path;
    } else {
        g_free(canon_path);
        return NULL;
    }
}

static int
string_compare(gconstpointer data, gconstpointer ref) {
    return strcmp((char*)data, (char*)ref);
}

/* Takes ownership of filepath and free it. */
static GSList*
add_unique(GSList* list, char* filepath, const char* source) {
    char* path = NULL;
    char* srcdir = NULL;
    char* concat = NULL;
    GSList* it = NULL;

    if (g_path_is_absolute(filepath)) {
        path = tip_canonicalize_absolute_filepath(filepath);
    }

    if (path == NULL) {
        if (source != NULL) {
            srcdir = g_path_get_dirname(source);
            concat = g_build_filename(srcdir, filepath, NULL);
            path = tip_canonicalize_absolute_filepath(concat);
            g_free(srcdir);
            g_free(concat);
        } else {
            // From command line.
            srcdir = g_get_current_dir();
            concat = g_build_filename(srcdir, filepath, NULL);
            path = tip_canonicalize_absolute_filepath(concat);
            g_free(srcdir);
            g_free(concat);
        }
    }

    if (path == NULL) {
        for (it = source_paths; it; it = it->next) {
            concat = g_build_filename((char*)it->data, filepath, NULL);
            path = tip_canonicalize_absolute_filepath(concat);
            g_free(concat);
            if (path) break;
        }
    }

    if (path == NULL) {
        fprintf(debug_log, "File [%s] does not exist!\n", filepath);
        g_free(filepath);
        return list;
    }
    g_free(filepath);

    it = g_slist_find_custom(list, path, string_compare);
    if (it != NULL) {
        g_free(path);
    } else {
        return g_slist_append(list, path);
    }

    return list;
}

/* Return owning data to free with g_free(). */
static char*
tip_real_path(const char* path) {
    char rp[PATH_MAX];
    if (realpath(path, rp) == NULL) {
        fprintf(debug_log, "Cannot resolve path for '%s'.\n", path);
        return NULL;
    }
    return g_strdup(rp);
}

static void
consolidate_proto_init(proto_t* proto, gpointer first_file)
{
    if (proto->name == NULL) {
        if (proto->short_name != NULL) {
            proto->name = g_strdup(proto->short_name);
        } else if (proto->abbrev != NULL) {
            proto->name = g_strdup(proto->abbrev);
        } else if (first_file != NULL) {
            char* path = first_file;
            proto->name = get_basename_thrift(path, false);
        } else {
            // No file to parse anyway.
            return;
        }
    }
    if (proto->short_name == NULL) {
        char* temp;
        size_t copy = strspn(proto->name,
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                "abcdefghijklmnopqrstuvwxyz"
                "0123456789_");
        temp = g_strndup(proto->name, copy);
        temp[copy] = '\0'; // MemorySanitizer complains that the trailing NUL is not initialized.
        proto->short_name = temp;
    }
    if (proto->abbrev == NULL) {
        proto->abbrev = g_ascii_strdown(proto->short_name, -1);
    }
    if (proto->author == NULL) {
        proto->author = g_strdup(KALIED_author);
    }
    if (proto->email == NULL) {
        proto->email = g_strdup(KALIED_email);
    }
    if (!proto->plugin || proto->spdx == NULL) {
        proto->spdx = g_strdup(SPDX_GPL2_0_plus);
    }
    if (proto->return_filter == NULL) {
        proto->return_filter = g_strdup(default_return_filter);
    }
}

static void
display_help(FILE* out)
{
    fprintf(out, "usage: kalied [options] thrift file list\n");
    fprintf(out, "\n");
    fprintf(out, "Basic options:\n");
    fprintf(out, "\t-h\n");
    fprintf(out, "\t--help           Show this help and exit\n");
    fprintf(out, "\n");
    fprintf(out, "\t--output <file>  Path to the source file to generate for the dissector (standard output by default).\n");
    fprintf(out, "\t--debug  <file>  Path to the log file to use during the generation (standard error by default).\n");
    fprintf(out, "\n");
    fprintf(out, "Wireshark contextual options:\n");
    fprintf(out, "\t--api-x.y        Sets the targeted Wireshark version.\n");
    fprintf(out, "\t--api-3.6        First Wireshark version covering every known types (as of Thrift 0.17.0).\n");
    fprintf(out, "\t--api-3.8        Support for UUID (Thrift 0.18.0+).\n");
    fprintf(out, "\t--api-4.2        Support for expert info for exceptions in reply.\n");
    fprintf(out, "\t--api-4.4        Support for raw dissectors (in progress).\n");
    fprintf(out, "\t                 Note: latest by default, even if not published yet.\n");
    fprintf(out, "\n");
    fprintf(out, "\t--builtin        \n");
    fprintf(out, "\t--plugin          (this is the default).\n");
    fprintf(out, "\n");
    fprintf(out, "Display filter options:\n");
    fprintf(out, "\t--matchcase      Use the same casing as used in the .thrift files (this is the default).\n");
    fprintf(out, "\t--lowercase      Force all names to use lowercase (Wireshark style).\n");
    fprintf(out, "\n");
    fprintf(out, "\t--no-prefix      Do not prefix the name of the types.\n");
    fprintf(out, "\t--filename       Use the file name as a prefix to the type.\n");
    fprintf(out, "\t--namespace      Use the \"wireshark\" namespace as a prefix to the type.\n");
    fprintf(out, "\n");
    fprintf(out, "\t--prefix-method  Add the prefix when the method name is used instead of a type.\n");
    fprintf(out, "\n");
    fprintf(out, "\t--filter <name>  Use <name> for the display filter of return types (\"result\" by default).\n");
    fprintf(out, "\n");
    fprintf(out, "Plugin customization:\n");
    fprintf(out, "\t--author  <Full Name>        Name of the author of the plugin.\n");
    fprintf(out, "\t--email   <email@address>    E-mail address of the author.\n");
    fprintf(out, "\t--name    <Protocol Name>    Full name of the protocol.\n");
    fprintf(out, "\t--short   <proto_short>      Short name of the protocol.\n");
    fprintf(out, "\t          Note: The first filename is used if --name and --short are omitted.\n");
    fprintf(out, "\t--license <SPDX identifier>  SPDX identifier of the license of the plugin.\n");
    fprintf(out, "\t          Note: This is GPL-2.0-or-later by default (as Wireshark).\n");
    fprintf(out, "\n");
    fprintf(out, "Expert code generation options:\n");
    fprintf(out, "\t--auto-switch    Add a switch to differentiate between return type and exception if necessary.\n");
    fprintf(out, "\t--force-switch   Always use the switch, even if the API level does not require it.\n");
    fprintf(out, "\n");
    fprintf(out, "\t--expert         Enable the creation of expert info in case of protocol exception.\n");
    fprintf(out, "\t--no-expert      Disable the creation of expert info (this is the default).\n");
    fprintf(out, "\n");
    fprintf(out, "\t--canary         Add an assertion to ensure the data structure is well formed.\n");
    fprintf(out, "\t--no-canary      Do not add the canary (this is the default).\n");
}

static void
initialize_protocol_parameters(parameters_t* parameters, int argc, char** argv)
{
    proto_t* proto = parameters->proto;
    char* author = NULL;
    char* email = NULL;
    char* name = NULL;
    char* short_name = NULL;
    char* abbrev = NULL;
    char* spdx = NULL;
    char* return_filter = NULL;
    char* output_path = NULL;
    char* debug = NULL;
    GSList* list = NULL;
    static int plugin = 1;
    static int expert = 0;
    static int sw_exc = 0;
    static int canary = 0;
    static int lowercase = 0;
    static int api_lvl = API_4_4; // Always target the latest known API level by default.
    static int prefix = FILENAME;
    static int prefix_method = 0;
    static struct option long_options[] = {
        /* These options set a flag. */
        { "api-3.6",       no_argument, &api_lvl, API_3_6  },
        { "api-3.8",       no_argument, &api_lvl, API_3_8  },
        { "api-4.0",       no_argument, &api_lvl, API_3_8  }, // No change in 4.0
        { "api-4.2",       no_argument, &api_lvl, API_4_2  },
        { "api-4.4",       no_argument, &api_lvl, API_4_4  },
        { "no-expert",     no_argument, &expert,         0 },
        { "expert",        no_argument, &expert,         1 },
        { "auto-switch",   no_argument, &sw_exc,         0 },
        { "force-switch",  no_argument, &sw_exc,         1 },
        { "builtin",       no_argument, &plugin,         0 },
        { "plugin",        no_argument, &plugin,         1 },
        { "no-canary",     no_argument, &canary,         0 },
        { "canary",        no_argument, &canary,         1 },
        { "matchcase",     no_argument, &lowercase,      0 },
        { "lowercase",     no_argument, &lowercase,      1 },
        { "no-prefix",     no_argument, &prefix, NO_PREFIX },
        { "filename",      no_argument, &prefix, FILENAME  },
        { "namespace",     no_argument, &prefix, NAMESPACE },
        { "prefix-method", no_argument, &prefix_method,  1 },
        /* These options don’t set a flag.
           We distinguish them by their indices. */
        { "author",  required_argument, NULL, 'a' },
        { "email",   required_argument, NULL, 'e' },
        { "name",    required_argument, NULL, 'n' },
        { "short",   required_argument, NULL, 's' },
        { "license", required_argument, NULL, 'l' },
        { "filter",  required_argument, NULL, 'f' },
        { "output",  required_argument, NULL, 'o' },
        { "debug",   required_argument, NULL, 'd' },
        { "help",    no_argument,       NULL, 'h' },
        { NULL,      no_argument,       NULL,  0  }
    };
    int option_index = 0;
    int c;

    while ((c = getopt_long(argc, argv, "pbh?a:e:n:s:l:f:o:d:", long_options, &option_index)) != -1) {
        switch (c) {
        case 0:
            break;
        case 'p':
            plugin = 1;
            break;
        case 'b':
            plugin = 0;
            break;
        case 'a':
            if (author != NULL) g_free(author);
            author = g_strdup(optarg);
            break;
        case 'e':
            if (email != NULL) g_free(email);
            email = g_strdup(optarg);
            break;
        case 'n':
            if (name != NULL) g_free(name);
            name = g_strdup(optarg);
            break;
        case 's':
            if (short_name != NULL) g_free(short_name);
            short_name = g_strdup(optarg);
            break;
        case 'l':
            if (spdx != NULL) g_free(spdx);
            spdx = g_strdup(optarg);
            break;
        case 'f':
            if (return_filter != NULL) g_free(return_filter);
            return_filter = g_strdup(optarg);
            break;
        case 'o':
            if (output_path != NULL) g_free(output_path);
            output_path = g_strdup(optarg);
            break;
        case 'd':
            if (debug != NULL) g_free(debug);
            debug = g_strdup(optarg);
            break;
        case '?':
        case 'h':
            display_help(stdout);
            exit(0);
        default:
            display_help(stderr);
            abort();
        }
    }

    if (debug != NULL) {
        debug_log = fopen(debug, "a");
    }
    if (optind < argc) {
        while (optind < argc) {
            char* path = tip_real_path(argv[optind++]);
            if (path != NULL) {
                list = add_unique(list, path, NULL);
            }
        }
    }

    proto->author = author;
    proto->email = email;
    proto->name = name;
    proto->short_name = short_name;
    proto->abbrev = abbrev;
    proto->spdx = spdx;
    proto->return_filter = return_filter;
    proto->plugin = plugin == 1;
    proto->canary = canary == 1;
    proto->lowercase = lowercase == 1;
    proto->prefix_method = prefix_method == 1;
    proto->prefix_type = prefix;
    proto->use_expert = expert == 1;
    proto->exc_switch = sw_exc == 1 || (proto->use_expert && api_lvl < API_4_2);
    proto->target_api = api_lvl;
    if (output_path != NULL) {
        parameters->output = fopen(output_path, "w");
    }
    parameters->file_list = list;
    consolidate_proto_init(proto, g_slist_nth_data(list, 0));
}


void
tip_include_file(thrift_idl_token_t* token, thrift_idl_state_t* state) {
    assert(state != NULL);
    assert(token != NULL);
    assert(token->v != NULL);
    int len = strlen(token->v);
    char* include = g_strndup(token->v + 1, len - 2);
    state->file_list = add_unique(state->file_list, include, state->current_file_path);
}

int main(int argc, char** argv) {
    FILE* out_file = stdout;
    proto_t proto;
    thrift_idl_state_t state;
    tip_types_t types;
    yyscan_t scanner;
    int status = 0;
    int token_id;
    parameters_t parameters = {
        .proto = &proto,
        .output = stdout,
        .file_list = NULL,
    };
    GSList* iter;
    memset(&proto, 0, sizeof(proto));
    memset(&state, 0, sizeof(state));
    memset(&types, 0, sizeof(types));
    debug_log = stderr;

    initialize_protocol_parameters(&parameters, argc, argv);
    state.file_list = parameters.file_list;
    state.types = &types;
    state.lowercase = proto.lowercase;
    state.supports_uuid = proto.target_api >= API_3_8;
    types.l.typedefs = g_hash_table_new(g_str_hash, g_str_equal);
    types.l.enumerations = g_hash_table_new(g_str_hash, g_str_equal);
    types.l.structures = g_hash_table_new(g_str_hash, g_str_equal);
    types.l.exceptions = g_hash_table_new(g_str_hash, g_str_equal);
    for (iter = state.file_list; iter != NULL && status == 0; iter = iter->next)
    {
        char* filepath = iter->data;
        FILE* fd = fopen(filepath, "r");
        if (!fd)
        {
            fprintf(debug_log, "Failed to open '%s'.\n", filepath);
            continue;
        }

        tip_reinit_state(&state);
        scanner = NULL;

        state.current_file_path = filepath;
        state.current_base_name = get_basename_thrift(filepath, proto.lowercase);
        status = thrift_idl_lex_init(&scanner);
        if (status != 0) {
            fprintf(debug_log, "Initialize Thrift IDL scanner failed: %d!\n", status);
            fclose(fd);
            status = 0x80000005;
            break;
        }
        thrift_idl_set_extra(&state, scanner);
        thrift_idl_restart(fd, scanner);
        state.scanner = scanner;
        while (state.grammar_error == ERROR_NONE && (token_id = thrift_idl_lex(scanner))) {
            ThriftIdlParser(state.parser, token_id, thrift_idl_get_extra(scanner)->token, &state);
        }
        fclose(fd);

        switch (state.grammar_error) {
            case ERROR_NONE:
                ThriftIdlParser(state.parser, 0, NULL, &state);
                break;
            case ERROR_SYNTAX:
                fprintf(debug_log, "Encountered a syntax error at line %d.\n", thrift_idl_get_extra(scanner)->token->line);
                status = 0x80000001;
                break;
            case ERROR_GRAMMAR:
                fprintf(debug_log, "Encountered a grammar error at line %d.\n", thrift_idl_get_extra(scanner)->token->line);
                status = 0x80000002;
                break;
            case ERROR_OUT_OF_RANGE:
                fprintf(debug_log, "Encountered an out of range integer at line %d.\n", thrift_idl_get_extra(scanner)->token->line);
                status = 0x80000003;
                break;
            case ERROR_ONEWAY_THROW:
                fprintf(debug_log, "Encountered a throwing oneway function at line %d.\n", thrift_idl_get_extra(scanner)->token->line);
                status = 0x80000004;
                break;
            case ERROR_UUID_NOT_SUPPORTED:
                fprintf(debug_log, "Encountered a field of type uuid (not supported before Wireshark 3.8) at line %d.\n", thrift_idl_get_extra(scanner)->token->line);
                status = 0x80000005;
                break;
            default:
                fprintf(debug_log, "Encountered another error: %d.\n", state.grammar_error);
                status = -2;
                break;
        }
    }
    set_proto_parameters(&types, &state);
    types.typedefs = g_slist_reverse(types.typedefs);
    types.enumerations = g_slist_reverse(types.enumerations);
    types.structures = g_slist_reverse(types.structures);
    types.exceptions = g_slist_reverse(types.exceptions);
    types.functions = g_slist_reverse(types.functions);
    annotation_data_t data = {
        .types = &types,
        .proto = &proto,
    };
    g_slist_foreach(types.functions, annotate_used_depth, &data);
    // Cleanup the scanner and parser
    tip_clear_state(&state);
    write_dissector(parameters.output, &types, &proto);
    final_cleanup(parameters.file_list, &proto, &types);
    return status;
}
