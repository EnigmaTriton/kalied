/* Copyright © 2021-2022 Triton Circonflexe
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sub-license,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/* Part 0: Options */
%option reentrant
%option never-interactive
/* yywrap is used to call multiple files in a chain, TBC. */
%option noyywrap
%option extra-type="thrift_idl_state_t *"
%option prefix="thrift_idl_"

%option yylineno
%option noinput
%option nounput

%option debug
/*%option verbose*/
%option warn
/*%option nodefault*/

/* Part 1: Definitions */
%{
/* thrift_idl_tokenizer.l
 *
 * Thrift IDL lexer (for .thrift files)
 * Copyright 2022, Triton Circonflexe <triton@kumal.info>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
//#include <math.h>
//#include <stdlib.h>
//#include <inttypes.h>

#include <glib.h>
#include <ws_diag_control.h>
#include "def_types.h"
#include "thrift_idl_parser.h"

DIAG_OFF_FLEX

//void thrift_idl_parser_error (TODO: See pbl_parser_error)

#define THRIFT_IDL_TOKENIZE(token_type) \
    thrift_idl_get_extra(yyscanner)->token = g_new0(thrift_idl_token_t, 1); \
    thrift_idl_get_extra(yyscanner)->token->v = g_strdup(yytext); \
    thrift_idl_get_extra(yyscanner)->token->line = thrift_idl_get_lineno(yyscanner); \
    return THRIFT_TOKEN_##token_type;
%}

/* Additional exclusive state when we are inside a C-style comment. */
%x CCOMMENT
    // %x SGLQUOTE
    // %x DBLQUOTE

/* Tokens */
int_constant ([-+]?([0-9]+|0x[0-9a-fA-F]+))
dbl_constant ([-+]?[0-9]+\.[0-9]*([Ee][-+]?[0-9]+)?)
uid_constant ([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})
gid_constant ("{"[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"}")
identifier   ([A-Za-z_](\.[A-Za-z_]|[0-9A-Za-z_])*)
whitespace   ([ \r\t]*)
newline      (\n)
sharpcomment ("#".*\n)
cppcomment   ("//".*\n)
splquote_str (['][^']*['])
dblquote_str (["][^"]*["])
reserved     (BEGIN|END|__CLASS__|__DIR__|__FILE__|__FUNCTION__|__LINE__|__METHOD__|__NAMESPACE__|abstract|alias|and|args|as|assert|begin|break|case|catch|class|clone|continue|declare|def|default|del|delete|do|dynamic|elif|else|elseif|elsif|end|enddeclare|endfor|endforeach|endif|endswitch|endwhile|ensure|except|exec|finally|float|for|foreach|from|function|global|goto|if|implements|import|in|inline|instanceof|interface|is|lambda|module|native|new|next|nil|not|or|package|pass|public|print|private|protected|raise|redo|rescue|retry|register|return|self|sizeof|static|super|switch|synchronized|then|this|throw|transient|try|undef|unless|unsigned|until|use|var|virtual|volatile|when|while|with|xor|yield)

%% /* Part 2: Rules. */
{sharpcomment} { /* Ignore */ }
{cppcomment}   { /* Ignore */ }
{whitespace}   { /* Ignore */ }
{newline}      { ++yylineno; /* Just increment the line number. */ }
{reserved}     { /* TODO: fprintf(stderr, "Reserved keyword: %s.", "plop"); return -1; // Parse error. */ }

    /* The above rule eats C comments on a single line, like this one. */
    /* The below condition takes care of the C comment that are
     * actually multi-line comments, like this one. */
<INITIAL>{
"/*"          { BEGIN(CCOMMENT); }
    /* [']           { BEGIN(SGLQUOTE); } */
    /* ["]           { BEGIN(DBLQUOTE); } */
}

<CCOMMENT>{
"*"+"/"       { BEGIN(INITIAL); }
[^*\n]+       { /* Eat comment chunk without asterisk. */ }
"*"+[^/*\n]+  { /* Eat all asterisks followed by anything but slash. */ }
"*"*\n        { ++yylineno; /* Eat ending asterisks. */ }
}
    /* <SGLQUOTE>{
    // [']           { BEGIN(INITIAL); THRIFT_IDL_TOKENIZE(LITERAL); }
    // [^'\n]+       { }
    // \n            { ++yylineno; }
    // }
    // <DBLQUOTE>{
    // ["]           { BEGIN(INITIAL); THRIFT_IDL_TOKENIZE(LITERAL); }
    // [^"\n]+       { }
    // \n            { ++yylineno; }
    // } */

","            THRIFT_IDL_TOKENIZE(COMMA);
";"            THRIFT_IDL_TOKENIZE(SEMICOLON);
":"            THRIFT_IDL_TOKENIZE(COLON);
"\*"           THRIFT_IDL_TOKENIZE(STAR);
"("            THRIFT_IDL_TOKENIZE(LPAREN);
")"            THRIFT_IDL_TOKENIZE(RPAREN);
"\["           THRIFT_IDL_TOKENIZE(LBRACKET);
"\]"           THRIFT_IDL_TOKENIZE(RBRACKET);
"{"            THRIFT_IDL_TOKENIZE(LCURLY);
"}"            THRIFT_IDL_TOKENIZE(RCURLY);
"<"            THRIFT_IDL_TOKENIZE(LANGLE);
">"            THRIFT_IDL_TOKENIZE(RANGLE);
"="            THRIFT_IDL_TOKENIZE(EQUAL);
    /* keywords */
include        THRIFT_IDL_TOKENIZE(INCLUDE);
cpp_include    THRIFT_IDL_TOKENIZE(CPP_INCLUDE);
namespace      THRIFT_IDL_TOKENIZE(NAMESPACE);
wireshark      THRIFT_IDL_TOKENIZE(WIRESHARK);
const          THRIFT_IDL_TOKENIZE(CONST);
typedef        THRIFT_IDL_TOKENIZE(TYPEDEF);
enum           THRIFT_IDL_TOKENIZE(ENUM);
xsd_all        THRIFT_IDL_TOKENIZE(XSD_ALL);
xsd_optional   THRIFT_IDL_TOKENIZE(XSD_OPTIONAL);
xsd_nillable   THRIFT_IDL_TOKENIZE(XSD_NILLABLE);
xsd_attrs      THRIFT_IDL_TOKENIZE(XSD_ATTRS);
struct         THRIFT_IDL_TOKENIZE(STRUCT);
union          THRIFT_IDL_TOKENIZE(UNION);
exception      THRIFT_IDL_TOKENIZE(EXCEPTION);
service        THRIFT_IDL_TOKENIZE(SERVICE);
extends        THRIFT_IDL_TOKENIZE(EXTENDS);
required       THRIFT_IDL_TOKENIZE(REQUIRED);
optional       THRIFT_IDL_TOKENIZE(OPTIONAL);
oneway         THRIFT_IDL_TOKENIZE(ONEWAY);
async          THRIFT_IDL_TOKENIZE(ONEWAY);
throws         THRIFT_IDL_TOKENIZE(THROWS);
void           THRIFT_IDL_TOKENIZE(VOID);
bool           THRIFT_IDL_TOKENIZE(BOOL);
byte           THRIFT_IDL_TOKENIZE(I8);
i8             THRIFT_IDL_TOKENIZE(I8);
i16            THRIFT_IDL_TOKENIZE(I16);
i32            THRIFT_IDL_TOKENIZE(I32);
i64            THRIFT_IDL_TOKENIZE(I64);
double         THRIFT_IDL_TOKENIZE(DOUBLE);
string         THRIFT_IDL_TOKENIZE(STRING);
binary         THRIFT_IDL_TOKENIZE(BINARY);
map            THRIFT_IDL_TOKENIZE(MAP);
set            THRIFT_IDL_TOKENIZE(SET);
list           THRIFT_IDL_TOKENIZE(LIST);
uuid           THRIFT_IDL_TOKENIZE(UUID);
cpp_type       THRIFT_IDL_TOKENIZE(CPP_TYPE);
false          THRIFT_IDL_TOKENIZE(BOOLEAN_CONST);
true           THRIFT_IDL_TOKENIZE(BOOLEAN_CONST);

{gid_constant} THRIFT_IDL_TOKENIZE(GUID_CONST);
{uid_constant} THRIFT_IDL_TOKENIZE(UUID_CONST);
{int_constant} THRIFT_IDL_TOKENIZE(INTEGER_CONST);
{dbl_constant} THRIFT_IDL_TOKENIZE(DOUBLE_CONST);
{identifier}   THRIFT_IDL_TOKENIZE(IDENTIFIER);
{splquote_str} THRIFT_IDL_TOKENIZE(LITERAL);
{dblquote_str} THRIFT_IDL_TOKENIZE(LITERAL);

. {
    //yyerror("Unexpected token in input: \"%s\" at line %d.\n", yytext, yylineno);
    /* Ignore it for now. */
}
%% /* Part 3: User code. */

DIAG_ON_FLEX

 // vim: tabstop=4 shiftwidth=4 expandtab:
